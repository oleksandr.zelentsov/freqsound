from bisect import insort
from dataclasses import dataclass, field

import pygame
from mido import Message, MetaMessage, MidiFile, MidiTrack, bpm2tempo, second2tick

from consts import SAMPLE_RATE
from notes.note import Note, NoteLength
from notes.sound import Sound
from notes.utils import full_note_difference
from utils import rescale
from waves.songs import BaseMetronome, Song, Track


def parse_midi(filename: str):
    mid = MidiFile(filename, clip=True)
    return mid


def play_midi(music_file):
    """
    stream music with mixer.music module in blocking manner
    this will stream the sound from disk while playing
    """
    try:
        _pygame_init()
    except Exception as e:
        print(e)
        return

    try:
        pygame.mixer.music.load(music_file)
    except pygame.error as e:
        print(e)
        return
    clock = pygame.time.Clock()
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
        # check if playback has finished
        clock.tick(10)

    _pygame_exit()


def _pygame_init():
    pygame.display.init()
    pygame.mixer.pre_init(
        frequency=SAMPLE_RATE,
        allowedchanges=pygame.AUDIO_ALLOW_ANY_CHANGE,
    )
    pygame.mixer.init()


def _pygame_exit():
    pygame.mixer.music.fadeout(1000)
    pygame.mixer.music.stop()
    pygame.display.quit()
    pygame.quit()


def note_to_midi(note: Note) -> int:
    intervals_from_a4 = full_note_difference(note.name, "a4")
    return int(69 - intervals_from_a4)


def midi_to_note(n: int) -> Note:
    intervals_from_a4 = 69 - n
    return Note("a4").transpose(-intervals_from_a4)


def volume_to_midi(velocity_percentage: float | int) -> int:
    res = int(rescale(velocity_percentage, 0, 200, 0, 127))
    if res < 0:
        return 0
    elif res > 127:
        return 127

    return res


def midi_to_volume(midi_num: int) -> float:
    return rescale(midi_num, 0, 127, 0, 200)


def note_length_to_midi_time(
    note_length: NoteLength,
    bpm: int,
    tick_tempo: int,
    ticks_per_beat: int,
) -> int:
    return int(
        second2tick(
            note_length.get_duration_s(bpm),
            ticks_per_beat,
            tick_tempo,
        )
    )


@dataclass
class MidiConverter:
    song: Song

    _mid: MidiFile = field(default=None, init=False)
    _bpm: float = field(default=None, init=False)
    _tick_tempo: int = field(default=None, init=False)

    def __post_init__(self):
        self._mid = MidiFile(type=1)
        self._bpm = self.song.bpm
        self._tick_tempo = bpm2tempo(self._bpm)

    def get_midi_track(self, channel_number: int, song_track: Track) -> MidiTrack:
        track = MidiTrack()
        track.append(Message("program_change", program=1, time=0))
        track.append(MetaMessage("set_tempo", tempo=self._tick_tempo))
        track.append(MetaMessage("time_signature", numerator=4, denominator=4))

        track.extend(
            self.get_messages(
                song_track.sounds * self.song.repeat,
                channel_number,
            )
        )

        return track

    def get_messages(self, sounds: list[Sound], channel_number: int) -> list[Message]:
        messages = []
        total_ticks = 0
        for sound in sounds:
            default_tick_duration = self.note_length_to_midi_time(sound.note_length)
            for note in sound.notes:
                note_duration = (
                    self.note_length_to_midi_time(note.custom_note_length)
                    if note.custom_note_length
                    else default_tick_duration
                )
                note_begin_time = total_ticks
                note_end_time = note_begin_time + note_duration

                insort(
                    a=messages,
                    x=Message(
                        "note_on",
                        channel=channel_number,
                        note=note_to_midi(note),
                        velocity=volume_to_midi(note.volume),
                        time=note_begin_time,
                    ),
                    key=lambda x: x.time,
                )
                insort(
                    a=messages,
                    x=Message(
                        "note_off",
                        channel=channel_number,
                        note=note_to_midi(note),
                        velocity=volume_to_midi(note.volume),
                        time=note_end_time,
                    ),
                    key=lambda x: x.time,
                )

            total_ticks += default_tick_duration

        for i in range(len(messages) - 1, 0, -1):
            message1 = messages[i - 1]
            message2 = messages[i]
            message2.time = message2.time - message1.time

        return messages

    def note_length_to_midi_time(self, note_length: NoteLength) -> int:
        return note_length_to_midi_time(
            note_length,
            self._bpm,
            self._tick_tempo,
            self._mid.ticks_per_beat,
        )

    def to_midi(self) -> MidiFile:
        self._mid.tracks.extend(
            self.get_midi_track(i, track)
            for i, track in enumerate(self.song.tracks)
            if not isinstance(track, BaseMetronome)
        )
        return self._mid


def print_midi_notes(fname: str):
    mf = MidiFile(fname)
    tracks: list[MidiTrack] = mf.tracks
    for track in tracks:
        for msg in filter(
            (lambda x: (not x.is_meta) and (x.type in ("note_on", "note_off"))),
            track,
        ):
            print(msg)
