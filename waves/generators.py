from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from decimal import Decimal
from typing import Callable

import numpy as np
from scipy import signal

from consts import SAMPLE_RATE
from notes.sound import Sound
from waves.manipulation import (
    WaveAdder,
    apply_volume,
    combine_waves,
    merge_waves,
    silence_latest_nonzeros,
)
from waves.utils import split_float


class BaseWaveGenerator(ABC):
    @abstractmethod
    def frequency_to_wave(
        self,
        frequency: Decimal,
        duration: int | float,
        fs=SAMPLE_RATE,
    ):
        ...

    def get_time_axis(self, duration: int | float, fs=SAMPLE_RATE) -> np.ndarray:
        """Generates a time dimension for the wave."""
        whole_duration, remainder_duration = split_float(duration)
        return np.linspace(
            0,
            duration,
            whole_duration * fs + round(remainder_duration * fs),
            False,
        )

    def build_scaled_track(
        self,
        *sounds: tuple[Sound],
        bpm: float = 120,
        transpose: int = 0,
        repeat: int = 1,
        scaler: Callable[[np.ndarray, int | float], np.ndarray],
        fs: int = SAMPLE_RATE,
    ) -> np.ndarray:
        """Scales an unscaled track which is made up from sounds."""
        max_value, wave = self.build_track(
            *sounds,
            bpm=bpm,
            transpose=transpose,
            repeat=repeat,
            fs=fs,
        )
        return scaler(wave, maximum=max_value)

    def build_track(
        self,
        *sounds: tuple[Sound],
        bpm: float = 120,
        transpose: int = 0,
        repeat: int = 1,
        fs: int = SAMPLE_RATE,
    ) -> tuple[float | int, np.ndarray]:
        wa = WaveAdder(fs=fs)
        for sound in sounds:
            sound: Sound = sound.transpose(transpose)
            wave = self.get_wave_from_sound(sound, bpm, fs)
            wa.add_wave(
                wave,
                should_sound_for=sound.note_length.get_duration_s(bpm),
            )
        max_val = np.max(np.abs(wa.res_wave)) if wa.res_wave.size else 0
        return max_val, merge_waves(*([wa.res_wave] * repeat))

    def get_wave_from_sound(
        self, sound: Sound, bpm: int, fs: int = SAMPLE_RATE
    ) -> np.ndarray:
        waves = self.get_waves_from_notes(sound, bpm=bpm, fs=fs)
        return combine_waves(*waves)

    def get_waves_from_notes(self, sound: Sound, bpm: int, fs: int = SAMPLE_RATE):
        waves = []
        default_duration = sound.note_length.get_duration_s(bpm)
        notes = [note for note in sound.notes if note]
        if not notes:  # generate silence
            time_dimension = self.get_time_axis(default_duration, fs)
            wave = np.zeros_like(time_dimension)
            return [wave]

        for note in notes:
            note_freq = note.freq
            length = (
                note.custom_note_length.get_duration_s(bpm)
                if note.custom_note_length
                else default_duration
            )
            wave = self.frequency_to_wave(
                note_freq,
                length,
                fs,
            )
            waves.append(apply_volume(wave, note.volume))

        return waves


class NormalTimingWaveGenerator(BaseWaveGenerator, ABC):
    def frequency_to_wave(
        self,
        frequency: Decimal,
        duration: int | float,
        fs=SAMPLE_RATE,
    ):
        frequency = float(frequency)
        time_dimension = self.get_time_axis(duration, fs)
        wave = self.get_wave_from_time(frequency, time_dimension)
        silence_latest_nonzeros(wave)  # TODO remove
        return wave

    @abstractmethod
    def get_wave_from_time(self, frequency: Decimal, time_dimension: np.ndarray):
        ...


@dataclass
class SineWaveGenerator(NormalTimingWaveGenerator):
    amplitude: float = 1
    phase: float = 0

    def get_wave_from_time(
        self,
        frequency: Decimal,
        time_dimension: np.ndarray,
    ) -> np.ndarray:
        return (
            np.sin(frequency * (time_dimension + self.phase) * 2 * np.pi)
            * self.amplitude
        )


@dataclass
class SquareWaveGenerator(NormalTimingWaveGenerator):
    amplitude: float = 1

    def get_wave_from_time(
        self,
        frequency: Decimal,
        time_dimension: np.ndarray,
    ) -> np.ndarray:
        return signal.square(frequency * time_dimension * 2 * np.pi) * self.amplitude


@dataclass
class StereoWaveGenerator(NormalTimingWaveGenerator):
    left_generator: BaseWaveGenerator
    right_generator: BaseWaveGenerator

    def get_wave_from_time(
        self,
        frequency: Decimal,
        time_dimension: np.ndarray,
    ) -> np.ndarray:
        return np.column_stack(
            [
                self.left_generator.get_wave_from_time(
                    frequency,
                    time_dimension,
                ),
                self.right_generator.get_wave_from_time(
                    frequency,
                    time_dimension,
                ),
            ]
        )


@dataclass
class DetunedWaveGenerator(NormalTimingWaveGenerator):
    phases: list["PhaseSetting"]
    generator: BaseWaveGenerator

    def __post_init__(self):
        if len(self.phases) > 200:
            raise ValueError("too many phases")

    def get_wave_from_time(
        self,
        frequency: Decimal,
        time_dimension: np.ndarray,
    ) -> np.ndarray:
        waves = []
        for phase in self.phases:
            wave = self.generator.get_wave_from_time(
                frequency=frequency,
                time_dimension=time_dimension + phase.phase,
            )
            wave = apply_volume(wave, phase.volume)
            waves.append(wave)

        return combine_waves(*waves)


@dataclass
class PhaseSetting:
    phase: float
    volume: int = 100


@dataclass
class CombinedWaveGenerator(BaseWaveGenerator):
    generators: list[BaseWaveGenerator]

    def frequency_to_wave(
        self,
        frequency: Decimal,
        duration: int | float,
        fs=SAMPLE_RATE,
    ) -> np.ndarray:
        raise NotImplementedError

    def build_track(
        self,
        *sounds: tuple[Sound],
        bpm: float = 120,
        transpose: int = 0,
        repeat: int = 1,
        fs: int = SAMPLE_RATE,
    ):
        maxes = []
        waves = []
        for generator in self.generators:
            max_value, wave = generator.build_track(
                *sounds,
                bpm=bpm,
                transpose=transpose,
                repeat=repeat,
                fs=fs,
            )
            waves.append(wave)
            maxes.append(max_value)

        return sum(maxes) * 1.1, combine_waves(*waves)


@dataclass
class PipelineWaveGenerator(BaseWaveGenerator):
    generator: BaseWaveGenerator
    pipeline: list[Callable] = field(default_factory=list)
    track_pipeline: list[Callable] = field(default_factory=list)

    def frequency_to_wave(
        self,
        frequency: Decimal,
        duration: int | float,
        fs=SAMPLE_RATE,
    ) -> np.ndarray:
        wave = self.generator.frequency_to_wave(frequency, duration, fs)
        pipeline = self.pipeline.copy()
        while pipeline:
            wave = pipeline.pop(0)(wave)

        return wave

    def build_track(
        self,
        *sounds: tuple[Sound],
        bpm: float = 120,
        transpose: int = 0,
        repeat: int = 1,
        fs: int = SAMPLE_RATE,
    ):
        max_value, wave = super().build_track(
            *sounds,
            bpm=bpm,
            transpose=transpose,
            repeat=repeat,
            fs=fs,
        )
        pipeline = self.track_pipeline.copy()
        while pipeline:
            wave = pipeline.pop(0)(wave)

        return max_value, wave


@dataclass
class OvertoneMachine(BaseWaveGenerator):
    generator: BaseWaveGenerator
    overtones: dict[float, int]

    def frequency_to_wave(
        self,
        frequency: Decimal,
        duration: int | float,
        fs=SAMPLE_RATE,
    ) -> np.ndarray:
        return combine_waves(
            *[
                apply_volume(
                    self.generator.frequency_to_wave(
                        frequency_multiplier * float(frequency),
                        duration,
                        fs,
                    ),
                    volume,
                )
                for frequency_multiplier, volume in self.overtones.items()
            ]
        )
