from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from functools import partial, reduce
from typing import Callable, Literal

import numpy as np

from consts import SAMPLE_RATE
from notes.note import Note, NoteLength
from notes.sound import Sound
from waves.generators import BaseWaveGenerator, PipelineWaveGenerator, SineWaveGenerator
from waves.manipulation import apply_volume, combine_waves
from waves.scalers import scale_any_bit

# @dataclass
# class AudioLength:
#     measures: float
#     bpm: int

#     @property
#     def total_seconds(self):


class BaseTrack(ABC):
    @abstractmethod
    def build_unscaled_track(
        self,
        bpm: float = 120,
        transpose: int = 0,
        repeat: int = 1,
        fs: int = SAMPLE_RATE,
    ):
        ...


@dataclass
class Track(BaseTrack):
    wave_generator: BaseWaveGenerator
    sounds: list[Sound]
    pipeline: list[Callable] = field(default_factory=list)

    def build_unscaled_track(
        self,
        bpm: float = 120,
        transpose: int = 0,
        repeat: int = 1,
        fs: int = SAMPLE_RATE,
    ):
        return reduce(
            lambda wave_props, transform_f: (wave_props[0], transform_f(wave_props[1])),
            self.pipeline,
            self.wave_generator.build_track(
                *self.sounds,
                bpm=bpm,
                transpose=transpose,
                repeat=repeat,
                fs=fs,
            ),
        )


@dataclass
class BaseMetronome(BaseTrack, ABC):
    volume: int = 40
    note: Note = Note("ab7")
    custom_note_length: NoteLength = NoteLength.D64
    _init_note: Note = field(init=False, default=None)
    wave_generator: BaseWaveGenerator = field(default=None)
    type: Literal[6] | Literal[4] = 4

    def get_note(self):
        return Note(
            self.note.name,
            self.note.freq,
            custom_note_length=self.custom_note_length,
        )

    def get_beat(self):
        if self.type == 6:
            return [
                Sound([self.get_note()], NoteLength.T12),
                Sound.rest(NoteLength.T12),
            ]
        return [
            Sound([self.get_note()], NoteLength.D32),
            Sound.rest(NoteLength.D32),
            Sound.rest(NoteLength.D16),
            Sound.rest(NoteLength.D8),
        ]

    def __post_init__(self):
        if not self.wave_generator:
            self.wave_generator = PipelineWaveGenerator(
                SineWaveGenerator(),
                track_pipeline=[
                    partial(apply_volume, volume=self.volume),
                ],
            )
        else:
            self.wave_generator = PipelineWaveGenerator(
                self.wave_generator,
                track_pipeline=[
                    partial(apply_volume, volume=self.volume),
                ],
            )

        self._init_note = self.note.transpose(-12)

    def build_unscaled_track(
        self,
        bpm: float = 120,
        transpose: int = 0,
        repeat: int = 1,
        fs: int = SAMPLE_RATE,
    ):
        sounds = self.get_sounds()
        return self.wave_generator.build_track(
            *sounds,
            bpm=bpm,
            repeat=repeat,
            fs=fs,
        )

    @abstractmethod
    def get_sounds(self, *args, **kwargs) -> list[Sound]:
        ...


@dataclass
class Metronome(BaseMetronome):
    measures: int = 0

    def get_sounds(self, *args, **kwargs) -> list[Sound]:
        measure_count = self.measures
        beat = self.get_beat()
        return beat * int(measure_count * self.type)


@dataclass
class SoundMetronome(BaseMetronome):
    sounds: list[Sound] = field(default_factory=list)

    def get_sounds(self, *args, **kwargs) -> list[Sound]:
        measure_count = sum(sound.note_length.value for sound in self.sounds)
        return self.get_beat() * int(measure_count * self.type)


@dataclass
class Song:
    tracks: list[Track | np.ndarray]
    bpm: float | int = 120
    transpose: int = 0
    repeat: int = 1
    scaler: Callable[[np.ndarray, int | float], np.ndarray] = scale_any_bit
    fs: int = SAMPLE_RATE

    def build_song(self) -> np.ndarray:
        track_waves = []
        total_max_value = 0
        for track in self.tracks:
            if isinstance(track, BaseTrack):
                max_value, wave = track.build_unscaled_track(
                    bpm=self.bpm,
                    transpose=self.transpose,
                    repeat=self.repeat,
                    fs=self.fs,
                )
                track_waves.append(wave)
                total_max_value += max_value
            else:
                track_waves.append(track)

        if not track_waves:
            return self.scaler(np.zeros(self.fs), total_max_value)
        wave = combine_waves(*track_waves)
        return self.scaler(wave, total_max_value)
