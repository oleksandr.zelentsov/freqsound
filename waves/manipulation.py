from dataclasses import dataclass, field
from functools import reduce

import numpy as np
from scipy.fftpack import fftfreq, irfft, rfft
from scipy.signal import butter, sosfilt

from consts import SAMPLE_RATE


def merge_waves(*waves):  # TODO account for stereo
    return np.concatenate([wave for wave in waves if wave.shape != (0,)])


def combine_2_waves(wave1, wave2):  # TODO account for stereo
    if len(wave1) < len(wave2):
        wave1 = merge_waves(wave1, np.zeros(len(wave2) - len(wave1)))
    elif len(wave1) > len(wave2):
        wave2 = merge_waves(wave2, np.zeros(len(wave1) - len(wave2)))

    return wave1 + wave2


def combine_waves(*waves):  # TODO account for stereo
    if not waves:
        raise ValueError("No waves to combine")
    return reduce(combine_2_waves, waves)


def repeat_wave(wave, n):
    if n == 1:
        return wave
    result = np.array([])
    for _ in range(n):
        result = merge_waves(result, wave)

    return result


def get_duration(wave, fs: int = SAMPLE_RATE) -> float:
    return len(wave) / fs


def apply_volume(wave, volume: int = 100):
    return wave * volume / 100


def tail_wave_min_length(wave, min_length_samples: int):
    if min_length_samples > len(wave):
        return

    begin = find_latest_crossing(wave[: -min_length_samples - 1])
    return wave[begin:]


def find_latest_crossing(wave):
    if wave.ndim != 1:
        raise ValueError()

    discrete_differences = np.diff(np.sign(wave))
    zero_crossings: np.ndarray = np.where(discrete_differences)[0]
    if zero_crossings.size == 0:
        return None
    return zero_crossings[-1]


def silence_latest_nonzeros(wave):
    last_zero_index = find_latest_crossing(wave)
    if last_zero_index is not None and len(wave) > (x := (last_zero_index + 1)):
        wave[x:] = np.zeros_like(wave[x:])
    return wave


def cut_low_pass_filter(wave, cut_frequency, fs=SAMPLE_RATE):
    s = wave
    dt = 1 / fs

    spectrogram = fftfreq(s.size, d=dt)
    f_signal = rfft(s)

    cut_f_signal = f_signal.copy()
    cut_f_signal[(np.abs(spectrogram) > cut_frequency)] = 0

    return irfft(cut_f_signal)


def smooth_band_pass_filter(
    wave,
    frequency: float,
    band_width: int,
    strength: int | float = 15,
    fs=SAMPLE_RATE,
):
    if band_width == 0:
        return wave

    min_frequency = max(frequency - band_width, 20)
    max_frequency = min(frequency + band_width, 20000)
    sos = butter(
        N=strength,
        Wn=[min_frequency, max_frequency],
        btype="bandpass",
        fs=fs,
        output="sos",
    )
    return sosfilt(sos, wave)


def smooth_low_pass_filter(
    wave,
    frequency: float,
    strength: int | float = 15,
    fs=SAMPLE_RATE,
):
    sos = butter(
        N=strength,
        Wn=frequency,
        btype="lowpass",
        fs=fs,
        output="sos",
    )
    return sosfilt(sos, wave)


def smooth_high_pass_filter(
    wave,
    frequency: float,
    strength: int | float = 15,
    fs=SAMPLE_RATE,
):
    sos = butter(
        N=strength,
        Wn=frequency,
        btype="highpass",
        fs=fs,
        output="sos",
    )
    return sosfilt(sos, wave)


def cut_high_pass_filter(wave, cut_frequency, fs=SAMPLE_RATE):
    s = wave
    dt = 1 / fs

    spectrogram = fftfreq(s.size, d=dt)
    f_signal = rfft(s)

    cut_f_signal = f_signal.copy()
    cut_f_signal[(np.abs(spectrogram) < cut_frequency)] = 0

    return irfft(cut_f_signal)


def decay_tail(wave: np.ndarray, tail_length: float = 0, fs: int = SAMPLE_RATE):
    if not tail_length:
        return wave

    decay_begin = int(tail_length * fs)
    if decay_begin > len(wave):
        raise ValueError(
            f"too long decay: {tail_length = }, {decay_begin = }, {len(wave) = }"
        )

    head, tail = wave[:-decay_begin], wave[-decay_begin:]
    multipliers = np.linspace(1, 0, len(tail))
    return merge_waves(head, np.multiply(tail, multipliers))


def add_decay(
    wave: np.ndarray,
    length: float = 0,
    length_to_window_coeff: float = 0.1,
    fs: int = SAMPLE_RATE,
):
    if length == 0:
        return wave

    window_samples = int(round(length_to_window_coeff * fs))
    length_samples = int(length * fs)
    window = tail_wave_min_length(wave, window_samples)
    window_times, window_fraction = divmod(length_samples, window_samples)
    if window_times == 0:
        window = np.array([])
    elif window_times == 1:
        lengthened_window = window
    else:
        lengthened_window = merge_waves(*([window] * window_times))

    lengthened_window = merge_waves(
        lengthened_window, window[: int(len(window) * window_fraction)]
    )
    multipliers = np.linspace(1, 0, len(lengthened_window))
    return merge_waves(wave, np.multiply(lengthened_window, multipliers))


def apply_adsr(
    wave: np.ndarray,
    a: float = 0,
    d: float = 0,
    s: float = 1,
    r: float = 0,
):
    pass


@dataclass
class WaveAdder:
    res_wave: np.ndarray = field(default_factory=lambda: np.array([]))
    fs: int = SAMPLE_RATE

    _last_offset: int = field(init=False, repr=False, default=0)

    def add_wave(
        self,
        wave: np.ndarray,
        should_sound_for: float | int,
    ) -> None:
        """
        - `wave` is a collection of samples
        - `should_sound_for` is in seconds
        """
        res_wave_samples = len(self.res_wave)
        wave_samples = len(wave)
        init_offset_samples = self._last_offset

        start_index = res_wave_samples - init_offset_samples
        end_index = start_index + wave_samples - 1

        if start_index < 0 and end_index <= 0:
            self.res_wave = merge_waves(
                wave,
                np.zeros(-start_index),
                self.res_wave,
            )
        elif start_index < 0 and (0 <= end_index <= (res_wave_samples - 1)):
            self.res_wave = merge_waves(
                wave[:-end_index],
                combine_waves(
                    wave[-end_index:],
                    self.res_wave[:end_index],
                ),
                self.res_wave[end_index:],
            )
        elif (0 <= start_index <= (res_wave_samples - 1)) and (
            0 <= end_index <= (res_wave_samples - 1)
        ):
            self.res_wave = merge_waves(
                self.res_wave[:start_index],
                combine_waves(
                    wave,
                    self.res_wave[start_index:end_index],
                ),
                self.res_wave[end_index:],
            )
        elif (
            0 <= start_index <= (res_wave_samples - 1)
        ) and end_index >= res_wave_samples:
            tail_length = end_index - res_wave_samples
            self.res_wave = merge_waves(
                self.res_wave[:start_index],
                combine_waves(
                    wave[:-tail_length],
                    self.res_wave[start_index:],
                ),
                wave[-tail_length:],
            )
        elif start_index >= res_wave_samples and end_index > res_wave_samples:
            self.res_wave = merge_waves(
                self.res_wave,
                np.zeros(start_index - res_wave_samples),
                wave,
            )
        else:
            raise ValueError(
                f"Unknown configuration: {res_wave_samples=}, {wave_samples=}, {start_index=}, {end_index=}"
            )

        res_wave_samples = len(self.res_wave)
        intended_end_index = start_index + int(should_sound_for * self.fs)
        if res_wave_samples != intended_end_index:
            self._last_offset = res_wave_samples - intended_end_index
        else:
            self._last_offset = 0
