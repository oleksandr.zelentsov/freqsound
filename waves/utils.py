import os

import numpy as np
from scipy.io.wavfile import read, write

from consts import SAMPLE_RATE, BIT_DEPTH


def split_float(duration: float):
    whole_duration = int(duration)
    remainder_duration = duration - whole_duration
    return whole_duration, remainder_duration


def play_wave(audio_wave: np.ndarray, fs=SAMPLE_RATE):
    print("playing wave...")
    if os.name == "nt":
        import winsound

        save_wav(audio_wave, "temp.wav", fs=fs)
        winsound.PlaySound("temp.wav", winsound.SND_FILENAME)
        os.unlink("temp.wav")
    else:
        import simpleaudio as sa

        # Start playback
        audio_wave = audio_wave.copy(order="C")
        play_obj = sa.play_buffer(audio_wave, audio_wave.ndim, BIT_DEPTH // 8, fs)

        # Wait for playback to finish before exiting
        play_obj.wait_done()
    print("finished playing")


def play_wave_file(filename):
    import simpleaudio as sa

    wo = sa.WaveObject.from_wave_file(filename)
    play_obj = wo.play()
    play_obj.wait_done()


def save_wav(audio_wave, filename, fs=SAMPLE_RATE):
    write(filename, fs, audio_wave)


def load_wav(filename):
    return read(filename)
