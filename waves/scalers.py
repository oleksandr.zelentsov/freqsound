from functools import partial

import numpy as np

from consts import BIT_DEPTH


def scale_any_bit(audio_wave, maximum=None, bits=BIT_DEPTH):
    # Ensure that highest value is in 16-bit range
    maximum = maximum or np.max(np.abs(audio_wave))
    audio_wave = audio_wave * (2 ** (bits - 1) - 1) / maximum
    # Convert to 16-bit data
    types_ = {
        8: np.int8,
        16: np.int16,
        32: np.int32,
        64: np.int64,
        256: np.float64,
    }
    key = max(key for key in types_.keys() if key <= bits)
    audio_wave = audio_wave.astype(types_[key])
    return audio_wave


scale_16bit = partial(scale_any_bit, bits=16)
