from auto_composers.config import AutoComposerV1PatternsConfig
from examples.wave_gens import FILTERED_SQUARE
from freeze_random_state import frozen_random_seed
from notes.generators.patterns.sound_generators import (
    PatternedSoundGenerator,
    RandomizedShiftPatternSwitcher,
)
from notes.generators.rhythm_generators import RhythmRepeater
from notes.generators.sound_generators import ScaleChordsSoundGenerator
from waves.songs import Song, Track


def main():
    config = AutoComposerV1PatternsConfig.from_env_vars()
    print(config.to_str())
    with frozen_random_seed(config.seed):
        gen = PatternedSoundGenerator(
            starting_note=config.root.transpose(12),
            scale=config.scale,
            pattern_switcher=RandomizedShiftPatternSwitcher(
                shift_range=config.shift_range,
            ),
            use_default_patterns=config.use_default_patterns,
            pattern_templates=config.patterns,
        )

        chord_gen = ScaleChordsSoundGenerator(
            root=config.root.transpose(0),
            scale=config.scale,
            rhythm_generator=RhythmRepeater(
                config.chord_rhythm,
            ),
        )

        audio_length = {"measure_count": config.measure_num, "bpm": config.bpm}
        sounds = list(gen.generate_sounds(**audio_length))
        chords = list(chord_gen.generate_sounds(**audio_length))
        return Song(
            [
                Track(
                    FILTERED_SQUARE,
                    sounds,
                    # pipeline=[partial(apply_volume, volume=50)],
                ),
                Track(
                    FILTERED_SQUARE,
                    chords,
                    # pipeline=[partial(apply_volume, volume=50)],
                ),
            ],
            bpm=config.bpm,
        )
