from functools import partial

from auto_composers.config import AutoComposerV1QuartetConfig
from examples.consts import SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE_BASS, SQUARE
from freeze_random_state import frozen_random_seed
from notes.chords import chord_sounds_to_bassline
from notes.generators.note_generators import RandomScaleOctaveNoteGenerator
from notes.generators.rhythm_generators import (
    RandomMeasureSubdivision,
    ShiftedRhythmGenerator,
)
from notes.generators.sound_generators import (
    ScaleChordsSoundGenerator,
    SimpleSingleRandomSoundGenerator,
)
from notes.note import NoteLength
from waves.manipulation import apply_volume
from waves.songs import Song, Track
from waves.utils import play_wave


def main():
    config = AutoComposerV1QuartetConfig.from_env_vars()
    print(config.to_str())
    with frozen_random_seed(config.seed):
        common_rhythm_gen = RandomMeasureSubdivision(
            starting_note_length=NoteLength.D2,
            minimum_note_length=config.melody_min_note_length,
            subdivision_count=2,
        )
        melody_1_generator = SimpleSingleRandomSoundGenerator(
            note_generator=RandomScaleOctaveNoteGenerator(
                scale=config.scale.lengthen(1),
                beginning_note=config.root,
                note_silence_probability=config.melody_note_silence_probability,
                max_scale_step=config.melody_max_scale_step,
                octave_jump_probability=config.octave_jump_probability,
            ),
            rhythm_generator=common_rhythm_gen,
        )
        melody_2_generator = SimpleSingleRandomSoundGenerator(
            note_generator=RandomScaleOctaveNoteGenerator(
                scale=config.scale.lengthen(1),
                beginning_note=config.root.transpose(12),
                note_silence_probability=config.melody_note_silence_probability,
                max_scale_step=config.melody_max_scale_step,
                octave_jump_probability=config.octave_jump_probability,
            ),
            rhythm_generator=ShiftedRhythmGenerator(
                common_rhythm_gen, config.melody_2_shift
            ),
        )
        chords_generator = ScaleChordsSoundGenerator(
            root=config.root.transpose(-12),
            rhythm_generator=RandomMeasureSubdivision(
                starting_note_length=NoteLength.D1,
                minimum_note_length=config.chords_min_note_length,
                subdivision_count=2,
                subdivision_rate=1 / 3,
            ),
            scale=config.scale,
        )
        melody_1_notes = list(
            melody_1_generator.generate_sounds(
                measure_count=config.measure_num,
                bpm=config.bpm,
            )
        )
        melody_2_notes = list(
            melody_2_generator.generate_sounds(
                measure_count=config.measure_num,
                bpm=config.bpm,
            )
        )
        chords = (
            list(
                chords_generator.generate_sounds(
                    measure_count=config.measure_num // 2,
                    bpm=config.bpm,
                )
            )
            * 2
        )
        bass_notes = chord_sounds_to_bassline(chords, -12)
        return Song(
            [
                Track(
                    SQUARE,
                    melody_1_notes,
                    pipeline=[
                        partial(apply_volume, volume=25),
                    ],
                ),
                Track(
                    SQUARE,
                    melody_2_notes,
                    pipeline=[
                        partial(apply_volume, volume=25),
                    ],
                ),
                # Track(
                #     FILTERED_SQUARE,
                #     chords,
                #     pipeline=[
                #         partial(apply_volume, volume=60),
                #     ],
                # ),
                Track(
                    FILTERED_SQUARE_BASS,
                    bass_notes,
                    pipeline=[
                        partial(apply_volume, volume=120),
                    ],
                ),
            ],
            **(SOUND_KWARGS | {"repeat": 1, "bpm": config.bpm}),
        )


if __name__ == "__main__":
    song = main()
    wave = song.build_song()
    play_wave(wave)
