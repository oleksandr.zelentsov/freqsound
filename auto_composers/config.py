from importlib import import_module
import os
import random
import re
from dataclasses import dataclass

from freeze_random_state import frozen_random_seed
from notes.note import Note, NoteLength, note_range
from notes.scales import BaseScale, ScaleMode, ScaleType


@dataclass
class BaseAutoComposerConfig:
    seed: int
    measure_num: int
    bpm_range: tuple[int, int]
    bpm: int
    root_range: tuple[str, str]
    root: Note
    scale: BaseScale
    scale_name: str
    mode: ScaleMode

    def to_str(self):
        return f"""
AC_SEED={hex(self.seed)}
AC_MEASURE_NUM={self.measure_num}
AC_BPM={self.bpm}
AC_ROOT={self.root.name}
AC_SCALE={self.scale_name}
AC_MODE={self.mode.name}
AC_BPM_RANGE={','.join(map(str, self.bpm_range))}
AC_ROOT_RANGE={','.join(self.root_range)}
"""

    @classmethod
    def seed_from_env_vars(cls):
        print(os.getenv("AC_SEED"))
        return (
            int(os.getenv("AC_SEED"), 16)
            if os.getenv("AC_SEED")
            else random.randint(0, int(1e10))
        )

    @classmethod
    def from_env_vars(cls):
        seed = cls.seed_from_env_vars()
        with frozen_random_seed(seed):
            measure_num = int(os.getenv("AC_MEASURE_NUM") or "12")

            bpm_range = os.getenv("AC_BPM_RANGE") or "75,144"
            bpm_range = tuple(map(int, bpm_range.split(",")))
            bpm = os.getenv("AC_BPM")
            bpm = int(bpm) if bpm else random.randint(*bpm_range)

            root_range = os.getenv("AC_ROOT_RANGE") or "a2,b3"
            root_range = root_range.split(",")
            root = os.getenv("AC_ROOT")
            root = Note(root) if root else random.choice(list(note_range(*root_range)))

            scale = os.getenv("AC_SCALE")
            if scale:
                # TODO support for direct scale
                scale = (
                    getattr(ScaleType, scale)
                    if re.match(r"[A-Z0-9_]+", scale)
                    else None
                )
            scale = scale or random.choice(list(ScaleType))
            scale_name = scale.name

            mode = os.getenv("AC_MODE")
            if mode:
                if re.match(r"[A-Z0-9_]+", mode):
                    mode = getattr(ScaleMode, mode)
                elif re.match(r"[1-7]", mode):
                    mode = ScaleMode(int(mode))
                else:
                    # TODO support for direct scale
                    mode = None
            mode = mode or ScaleMode(random.randint(1, 7))
            scale = scale.use_mode(mode)
            kwargs = cls.from_env_vars_custom()
            kwargs_cls = {
                "seed": seed,
                "measure_num": measure_num,
                "bpm": bpm,
                "root": root,
                "scale": scale,
                "mode": mode,
                "bpm_range": bpm_range,
                "root_range": root_range,
                "scale_name": scale_name,
            } | kwargs
        return cls(**kwargs_cls)

    @classmethod
    def from_env_vars_custom(cls):
        return {}


@dataclass
class AutoComposerV1Config(BaseAutoComposerConfig):
    chords_min_note_lengths: list[NoteLength]
    chords_min_note_length: NoteLength
    melody_min_note_lengths: list[NoteLength]
    melody_min_note_length: NoteLength
    melody_silence_probabilities: list[float]
    melody_note_silence_probability: float
    melody_max_scale_step: int
    octave_jump_probability: float

    @classmethod
    def from_env_vars_custom(cls):
        melody_max_scale_step = (
            int(os.getenv("AC_MELODY_MAX_SCALE_STEP"))
            if os.getenv("AC_MELODY_MAX_SCALE_STEP")
            else None
        )
        melody_max_scale_step = (
            melody_max_scale_step
            if melody_max_scale_step is not None
            else random.randint(0, 12)
        )

        octave_jump_probability = (
            float(os.getenv("AC_MELODY_OCTAVE_JUMP_PROBABILITY"))
            if os.getenv("AC_MELODY_OCTAVE_JUMP_PROBABILITY") is not None
            else None
        )
        octave_jump_probability = octave_jump_probability or 0.08

        chords_min_note_lengths = os.getenv("AC_CHORDS_MIN_NOTE_LENGTHS")
        if chords_min_note_lengths:
            chords_min_note_lengths = tuple(
                map(
                    lambda x: getattr(NoteLength, x.upper()),
                    chords_min_note_lengths.split(","),
                )
            )
        chords_min_note_length = random.choice(
            chords_min_note_lengths
            or (
                NoteLength.D4,
                NoteLength.D2,
            )
        )

        melody_min_note_lengths = os.getenv("AC_MELODY_MIN_NOTE_LENGTHS")
        if melody_min_note_lengths:
            melody_min_note_lengths = tuple(
                map(
                    lambda x: getattr(NoteLength, x.upper()),
                    melody_min_note_lengths.split(","),
                )
            )
        melody_min_note_length = random.choice(
            melody_min_note_lengths
            or (
                NoteLength.T6,
                NoteLength.D8,
                NoteLength.T12,
                NoteLength.D16,
                NoteLength.T24,
            )
        )

        melody_silence_probabilities = os.getenv("AC_MELODY_NOTE_SILENCE")
        if melody_silence_probabilities:
            melody_silence_probabilities = tuple(
                map(float, melody_silence_probabilities.split(","))
            )
        melody_note_silence_probability = random.choice(
            melody_silence_probabilities
            or (
                0.5,
                0.6,
                0.8,
                0.825,
                0.85,
                0.9,
            )
        )

        return dict(
            melody_silence_probabilities=melody_silence_probabilities,
            melody_note_silence_probability=melody_note_silence_probability,
            melody_min_note_lengths=melody_min_note_lengths,
            melody_min_note_length=melody_min_note_length,
            chords_min_note_lengths=chords_min_note_lengths,
            chords_min_note_length=chords_min_note_length,
            octave_jump_probability=octave_jump_probability,
            melody_max_scale_step=melody_max_scale_step,
        )

    def to_str(self):
        return (
            super().to_str().strip()
            + "\n"
            + f"""
AC_MELODY_NOTE_SILENCE_PROBABILITY={self.melody_note_silence_probability}
AC_MELODY_MIN_NOTE_LENGTH={self.melody_min_note_length}
AC_CHORDS_MIN_NOTE_LENGTH={self.chords_min_note_length}
AC_OCTAVE_JUMP_PROBABILITY={self.octave_jump_probability}
AC_MELODY_MAX_SCALE_STEP={self.melody_max_scale_step}
""".strip()
        )


@dataclass
class AutoComposerV1PatternsConfig(BaseAutoComposerConfig):
    use_default_patterns: bool
    shift_range: tuple[int, int]
    patterns_file_path: str
    patterns: list
    chord_rhythm: list

    @classmethod
    def from_env_vars_custom(cls):
        patterns_file_path = os.getenv("AC_PATTERNS_FILE_PATH")
        patterns = None  # handle defaults
        chord_rhythm = [NoteLength.D4]
        if patterns_file_path:
            patterns_module = import_module(patterns_file_path)
            patterns = patterns_module.patterns
            chord_rhythm = patterns_module.chord_rhythm

        return {
            "use_default_patterns": bool(
                int(os.getenv("AC_USE_DEFAULT_PATTERNS") or "0")
            ),
            "shift_range": tuple(
                int(x) for x in (os.getenv("AC_SHIFT_RANGE") or "-1,1").split(",")
            ),
            "patterns_file_path": os.getenv("AC_PATTERNS_FILE_PATH"),
            "patterns": patterns,
            "chord_rhythm": chord_rhythm,
        }

    def to_str(self):
        return (
            super().to_str().strip()
            + "\n"
            + f"""
AC_USE_DEFAULT_PATTERNS={int(self.use_default_patterns)}
AC_SHIFT_RANGE={"%s,%s" % self.shift_range}
AC_PATTERNS_FILE_PATH={self.patterns_file_path}
""".strip()
        )


@dataclass
class AutoComposerV1QuartetConfig(AutoComposerV1Config):
    melody_2_shift: NoteLength

    def to_str(self):
        return f"{super().to_str()}\nAC_MELODY_2_SHIFT={self.melody_2_shift}"

    @classmethod
    def from_env_vars_custom(cls):
        res = super().from_env_vars_custom()

        if melody_2_shift := os.getenv("AC_MELODY_2_SHIFT"):
            melody_2_shift = getattr(NoteLength, melody_2_shift, None)

        return {
            "melody_2_shift": melody_2_shift,
        } | res
