from importlib import import_module
from typing import Callable

from consts import MAIN_LOCATION, MAIN_OUTPUT
from waves.songs import Song
from waves.utils import play_wave, save_wav


def get_main(f_full_name: str) -> Callable[[], Song]:
    if not f_full_name:
        raise ValueError()
    package_name, fname = f_full_name.rsplit(".", 1)
    main_mod = import_module(package_name)
    return getattr(main_mod, fname)


main = get_main(MAIN_LOCATION)


if __name__ == "__main__":
    wave = main().build_song()
    save_wav(wave, MAIN_OUTPUT)
    play_wave(wave)
