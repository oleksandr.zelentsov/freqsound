import os
from dataclasses import dataclass
from importlib import import_module
from typing import Callable

import pytest
from numpy import array_equal

from notes.chords import chord_name_from_note_names
from notes.note import Note
from notes.scales import Scale, ScaleType
from waves.midi import midi_to_note, note_to_midi
from waves.songs import Song
from waves.utils import load_wav, save_wav


@dataclass
class SongExample:
    callable: Callable[[], Song]
    module: str
    audio_path: str

    @classmethod
    def from_callable(cls, callable):
        module_name = callable.__module__
        _, last_name = module_name.rsplit(".", 1)
        return cls(
            callable=callable,
            module=module_name,
            audio_path=f"./examples/test_data/{last_name}.wav",
        )

    @property
    def data_exists(self):
        return os.path.isfile(self.audio_path)

    def save_data(self):
        song = self.callable()
        save_wav(song.build_song(), self.audio_path)

    def __str__(self) -> str:
        return str(self.module)


def get_song_examples() -> list[SongExample]:
    result = []
    for fname in os.listdir(os.path.join(os.getcwd(), "examples/songs")):
        try:
            fname, ext = fname.rsplit(".", 1)
        except ValueError:
            continue

        if ext != "py":
            print(f'skipping file with extension "{ext}"')
            continue

        try:
            mod = import_module(f"examples.songs.{fname}")
        except RuntimeError as e:
            print(f'cannot import "examples.songs.{fname}" due to error')
            raise e

        if main := getattr(mod, "main", None):
            result.append(SongExample.from_callable(main))

    return result


@pytest.fixture
def song_examples() -> list[SongExample]:
    return get_song_examples()


def test_songs(song_examples, subtests):
    assert song_examples
    for song_example in song_examples:
        with subtests.test(msg=str(song_example)):
            _, wave_should = load_wav(song_example.audio_path)
            song = song_example.callable()
            wave_is = song.build_song()
            assert array_equal(wave_should, wave_is)


def test_lengthen_scale():
    s1 = ScaleType.NATURAL_MINOR
    s2 = s1.lengthen(1)

    assert s2.value == [0, 2, 3, 5, 7, 8, 11, 12]

    s1 = Scale([0, 1, 2])
    s2 = s1.lengthen(4)

    assert s2.value == [0, 1, 2, 12, 13, 14, 24]


midi_test_cases = [
    [Note("a4"), 69],
    [Note("a#4"), 70],
    [Note("a3"), 69 - 12],
    [Note("a2"), 69 - 24],
    [Note("b5"), 69 + 14],
]


@pytest.mark.parametrize(
    [
        "note",
        "midi_number",
    ],
    midi_test_cases,
)
def test_note_to_midi(note, midi_number):
    assert note_to_midi(note) == midi_number


@pytest.mark.parametrize(
    [
        "note",
        "midi_number",
    ],
    midi_test_cases,
)
def test_midi_to_note(note, midi_number):
    assert midi_to_note(midi_number) == note


@pytest.mark.parametrize(
    ["root_note", "scale_type", "key", "note"],
    (
        (Note("c3"), ScaleType.MAJOR, -13, Note("c1")),
        (Note("c3"), ScaleType.MAJOR, -12, Note("d1")),
        (Note("c3"), ScaleType.MAJOR, -11, Note("e1")),
        (Note("c3"), ScaleType.MAJOR, -10, Note("f1")),
        (Note("c3"), ScaleType.MAJOR, -9, Note("g1")),
        (Note("c3"), ScaleType.MAJOR, -8, Note("a1")),
        (Note("c3"), ScaleType.MAJOR, -7, Note("b1")),
        (Note("c3"), ScaleType.MAJOR, -6, Note("c2")),
        (Note("c3"), ScaleType.MAJOR, -5, Note("d2")),
        (Note("c3"), ScaleType.MAJOR, -4, Note("e2")),
        (Note("c3"), ScaleType.MAJOR, -3, Note("f2")),
        (Note("c3"), ScaleType.MAJOR, -2, Note("g2")),
        (Note("c3"), ScaleType.MAJOR, -1, Note("a2")),
        (Note("c3"), ScaleType.MAJOR, 0, Note("b2")),
        (Note("c3"), ScaleType.MAJOR, 1, Note("c3")),
        (Note("c3"), ScaleType.MAJOR, 2, Note("d3")),
        (Note("c3"), ScaleType.MAJOR, 3, Note("e3")),
        (Note("c3"), ScaleType.MAJOR, 4, Note("f3")),
        (Note("c3"), ScaleType.MAJOR, 5, Note("g3")),
        (Note("c3"), ScaleType.MAJOR, 6, Note("a3")),
        (Note("c3"), ScaleType.MAJOR, 7, Note("b3")),
        (Note("c3"), ScaleType.MAJOR, 8, Note("c4")),
        (Note("c3"), ScaleType.MAJOR, 9, Note("d4")),
        (Note("c3"), ScaleType.MAJOR, 10, Note("e4")),
        (Note("c3"), ScaleType.MAJOR, 11, Note("f4")),
        (Note("c3"), ScaleType.MAJOR, 12, Note("g4")),
        (Note("c3"), ScaleType.MAJOR, 13, Note("a4")),
        (Note("c3"), ScaleType.MAJOR, 14, Note("b4")),
        (Note("c3"), ScaleType.MAJOR, 15, Note("c5")),
    ),
)
def test_scale_lookup(root_note, scale_type, key, note):
    assert root_note.transpose(scale_type[key]).name.lower() == note.name.lower()


@pytest.mark.parametrize(
    ["note_names", "chord_name"],
    (
        [("c5", "d#5", "g5"), "Cm"],
        [("c5", "d5", "g5"), "Csus2"],
        [("c5", "e5", "g5"), "C"],
        [("c5", "f5", "g5"), "Csus4"],
        [("e4", "g4", "a#4"), "Edim"],
        [("a#4", "d5", "f5"), "A#"],
        [("f4", "g4", "c5"), "Fsus2"],
        [("f4", "a#4", "c5"), "Fsus4"],
    ),
)
def test_chord_name_from_note_names(note_names, chord_name):
    assert chord_name_from_note_names(*note_names) == chord_name
