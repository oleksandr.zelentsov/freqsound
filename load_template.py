import os

import yaml

from examples.wave_gens import FILTERED_SQUARE, FILTERED_SQUARE_BASS, SQUARE
from notes.generators.rhythm_generators import RandomMeasureSubdivision, RhythmRepeater
from notes.generators.sound_generators import (
    ScaleChordsSoundGenerator,
    ScaleDegreesChordsSoundGenerator,
    SimpleSingleRandomSoundGenerator,
)
from waves.songs import Song

YAML_LOCATION = os.getenv("YAML_LOCATION") or "song.yaml"

RHYTHM_GENERATOR_CLASSES = {
    "subdivide": RandomMeasureSubdivision,
    "repeat": RhythmRepeater,
}
SOUND_GENERATOR_CLASSES = {
    "note-rhythm": SimpleSingleRandomSoundGenerator,
    "scale-chords": ScaleChordsSoundGenerator,
    "scale-degree-chords": ScaleDegreesChordsSoundGenerator,
}
WAVE_GENERATOR_CLASSES = {
    "square": SQUARE,
    "filtered-square": FILTERED_SQUARE,
    "filtered-square-bass": FILTERED_SQUARE_BASS,
}


def parse_wave_gen_class(name):
    return WAVE_GENERATOR_CLASSES[name]


def parse_track(obj: dict):
    wave_gen_class = parse_wave_gen_class(obj["wg"])


def main():
    with open(YAML_LOCATION, "r") as fp:
        config = yaml.load(fp)

    tracks = []
    song_kwargs = {
        "repeat": config.get("repeat", 1),
        "bpm": config.get("bpm", 120),
        "transpose": config.get("transpose", 0),
    }

    song = Song(tracks=tracks, **song_kwargs)
