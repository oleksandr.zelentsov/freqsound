def rescale(X, A, B, C, D, force_float=False):
    retval = ((float(X - A) / (B - A)) * (D - C)) + C
    if not force_float and all(map(lambda x: type(x) == int, [X, A, B, C, D])):
        return int(round(retval))
    return retval
