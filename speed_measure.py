from statistics import mean
from time import time

from prettytable import PrettyTable
from scipy.stats import tstd
from uncertainties import ufloat

from consts import SAMPLE_RATE
from tests import get_song_examples


def get_measurement_result(measurements):
    return ufloat(
        mean(measurements),
        tstd(measurements),
    )


def get_decimal_places(n):
    x = n.s
    result = 0
    while int(x) == 0 and x != 0:
        x = x * 10
        result += 1

    return result


if __name__ == "__main__":
    songs = get_song_examples()
    all_times = []
    results = []
    begin_time = time()
    for song in songs:
        song_result = song.callable().build_song()
        finish_time = time()
        generation_took = finish_time - begin_time
        all_times.append(generation_took)
        wave_duration = round(len(song_result) / SAMPLE_RATE, 1)
        results.append(
            (
                song.module,
                generation_took,
                wave_duration,
                generation_took / wave_duration * 10,
            )
        )
        begin_time = time()

    results = sorted(results, key=lambda x: x[1], reverse=True)
    pretty_table = PrettyTable()
    pretty_table.field_names = [
        "example name",
        "generation took [s]",
        "audio length [s]",
        "est. gen time for 10 seconds [s]",
    ]
    pretty_table.align = "l"
    pretty_table.align["audio length [s]"] = "c"
    pretty_table.add_rows(results)
    print(pretty_table)
    print()
    avg = get_measurement_result(all_times)
    dec_places = get_decimal_places(avg)
    print(f"average {round(avg.n, dec_places)}±{round(avg.s, dec_places)}")
    print("all took", sum(all_times))
