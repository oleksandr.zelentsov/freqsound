import os
import random
from contextlib import contextmanager
from functools import wraps


def freeze_random_seed(val=None, silent=False):
    def _decorator(decorated_func):
        @wraps(decorated_func)
        def decorator(*args, **kwargs):
            st = random.getstate()
            new_seed = random.randint(0, int(1e10))
            old_envvar = os.getenv("AC_SEED")
            if val:
                new_seed = val

            random.seed(new_seed)
            os.environ["AC_SEED"] = hex(new_seed)
            return_val = decorated_func(*args, **kwargs)
            if not old_envvar:
                del os.environ["AC_SEED"]
            else:
                os.environ["AC_SEED"] = old_envvar
            if not silent:
                print("seed=", hex(new_seed), sep="")

            random.setstate(st)
            return return_val

        return decorator

    return _decorator


@contextmanager
def frozen_random_seed(val=None, silent=True):
    st = random.getstate()
    new_seed = random.randint(0, int(1e10))
    if val:
        new_seed = val

    random.seed(new_seed)
    yield new_seed
    if not silent:
        print("seed=", hex(new_seed), sep="")

    random.setstate(st)
