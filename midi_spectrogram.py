from multiprocessing import Process

from main import main
from midi_main import filename
from spectrogram import SpectrogramPlotter
from waves.midi import MidiConverter, play_midi
from waves.songs import Song


def generate_midi(song: Song):
    midi = MidiConverter(song)
    mf = midi.to_midi()
    mf.save(filename)
    play_midi(filename)


def midi_and_spectrogram():
    song = main()
    wave = song.build_song()
    fs = song.fs

    plot_process = Process(
        target=SpectrogramPlotter(
            fs=fs,
            nfft=2048,
        ).plot,
        args=(wave,),
    )
    plot_process.start()
    generate_midi(song)
    plot_process.join()


if __name__ == "__main__":
    midi_and_spectrogram()
