from tests import SongExample, get_song_examples

if __name__ == "__main__":
    song_examples: list[SongExample] = get_song_examples()
    for song in song_examples:
        if not song.data_exists:
            print("creating test data for song", song)
            song.save_data()
