import os
from dataclasses import dataclass, field
from multiprocessing import Process
from typing import Iterable, Literal

import numpy as np
from matplotlib import pyplot
from matplotlib.axes import Axes
from matplotlib.axis import Axis
from matplotlib.scale import LogScale

from consts import SAMPLE_RATE
from main import main
from notes.scales import BaseScale
from waves.manipulation import merge_waves
from waves.utils import load_wav, play_wave

FILE_TO_READ = os.getenv("WAVE_FILE") if os.getenv("READ_WAVE_FILE") == "1" else None


@dataclass(frozen=True)
class SpectrogramPlotter:
    fs: int | float = SAMPLE_RATE
    top_freq: float | int = 20000
    bottom_freq: float | int = 100
    pad_to: int | None = None
    nfft: int = 256
    noverlap: int = int(nfft / 2)
    figsize: tuple[int, int] = (15, 8)
    detrend: Literal["none"] | Literal["mean"] | Literal["linear"] = "none"
    mode: Literal["default"] | Literal["psd"] | Literal["magnitude"] | Literal[
        "angle"
    ] | Literal["phase"] = "magnitude"
    plot_wave: bool = False

    _axes_items: Iterable[Axes] | None = field(
        default=None,
        init=False,
        compare=False,
        hash=False,
    )
    _channels: Iterable[np.ndarray] | None = field(
        default=None,
        init=False,
        compare=False,
        hash=False,
    )

    def __post_init__(self):
        object.__setattr__(self, "noverlap", int(self.nfft / 2))

    def plot(self, wave: np.ndarray):
        self.plot_specgrams(wave)
        pyplot.xlabel("song time [s]")

        if self.plot_wave:
            print("xlim 1", pyplot.xlim())
            wave_axes: Axes = self._axes_items[-1]
            scaled_wave = merge_waves(
                *(
                    np.mean(wave[i : i + self.fs], keepdims=True)
                    for i in range(0, len(wave), int(self.fs / 100))
                )
            )
            wave_axes.plot(scaled_wave, alpha=0.3)
            print("xlim 2", pyplot.xlim())

        pyplot.show()

    def plot_specgrams(self, wave: np.ndarray):
        self._init_subplots(wave)
        for axes, channel in zip(self._axes_items, self._channels):
            axes.specgram(channel, **self.specgram_kwargs)
            axes.set_yscale(self.get_yscale(axes.get_yaxis()))
            axes.set_ylabel("frequency [Hz]")
            axes.set_ylim(*self.ylim)

    def _init_subplots(self, wave: np.ndarray):
        object.__setattr__(
            self,
            "_channels",
            ([wave[:, i] for i in range(wave.ndim)] if wave.ndim > 1 else [wave]),
        )
        _, axes_items = pyplot.subplots(
            len(self._channels) + (1 if self.plot_wave else 0),
            sharex="all",
            **self.figure_kwargs,
        )
        object.__setattr__(
            self,
            "_axes_items",
            (axes_items if isinstance(axes_items, Iterable) else [axes_items]),
        )

    def get_yscale(self, axis: Axis) -> BaseScale:
        return LogScale(axis, base=10)

    @property
    def window(self) -> np.ndarray:
        return np.bartlett(self.nfft)

    @property
    def specgram_kwargs(self) -> dict:
        return {
            "Fs": self.fs,
            "scale": "linear",
            "mode": self.mode,
            "NFFT": self.nfft,
            "noverlap": self.noverlap,
            "pad_to": self.pad_to,
            "window": self.window,
            "detrend": self.detrend,
            "cmap": "nipy_spectral",
        }

    @property
    def figure_kwargs(self) -> dict:
        return {"figsize": self.figsize}

    @property
    def ylim(self) -> tuple[int, int]:
        return (self.bottom_freq, self.top_freq)


if __name__ == "__main__":
    if not FILE_TO_READ:
        song = main()
        wave = song.build_song()
        fs = song.fs
    else:
        fs, wave = load_wav(FILE_TO_READ)

    plot_process = Process(
        target=SpectrogramPlotter(
            fs=fs,
            nfft=2048,
        ).plot,
        args=(wave,),
    )
    plot_process.start()
    play_wave(wave, fs)
    plot_process.join()
