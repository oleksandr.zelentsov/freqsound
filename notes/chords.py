import re
from dataclasses import dataclass
from enum import Enum
from itertools import islice

import numpy as np

from notes.note import BaseNoteLength, Note
from notes.scales import Scale
from notes.sound import Sound
from notes.utils import (
    full_note_difference,
    generate_scale,
    invert_intervals,
    note_difference_octave,
    note_name_to_frequency,
)

CHORD_PATTERN = re.compile(r"([A-Ga-g]{1}[\#b]?)(.*)")


@dataclass
class ChordProperties:
    intervals: tuple[int]
    notation: str


class BaseChord:
    def as_sound(
        self,
        root: str | float,
        note_length: BaseNoteLength,
        inversion: int = 0,
        bpm: float = 120,
        base_volume: int = 100,
        fall_volume: int = 80,
        reverse_fall: bool = True,
        additional_notes: int = 0,
    ) -> Sound:
        root_freq = note_name_to_frequency(root) if isinstance(root, str) else root
        intervals = invert_intervals(self.value.intervals, inversion_num=inversion)
        freqs = list(
            islice(
                generate_scale(root_freq, steps=intervals),
                len(intervals) + additional_notes,
            )
        )
        volumes = (
            np.linspace(fall_volume, base_volume, len(freqs))
            if reverse_fall
            else np.linspace(base_volume, fall_volume, len(freqs))
        )
        return Sound(
            [
                Note.from_freq(freq, volume=volume)
                for freq, volume in zip(freqs, volumes)
            ],
            note_length=note_length,
        )


@dataclass
class Chord(BaseChord):
    value: ChordProperties


class ChordType(BaseChord, Enum):
    # basic triads
    MAJOR = ChordProperties((0, 4, 7), "")
    MINOR = ChordProperties((0, 3, 7), "m")
    AUGMENTED = ChordProperties((0, 4, 8), "aug")
    DIMINISHED = ChordProperties((0, 3, 6), "dim")

    # suspended triads
    SUSPENDED_2 = ChordProperties((0, 2, 7), "sus2")
    SUSPENDED_4 = ChordProperties((0, 5, 7), "sus4")

    # 7th extensions
    MAJOR_7 = ChordProperties((0, 4, 7, 11), "maj7")
    MINOR_7 = ChordProperties((0, 3, 7, 10), "m7")
    DOMINANT_7 = ChordProperties((0, 4, 7, 10), "7")

    # 6th extensions
    MAJOR_6 = ChordProperties((0, 4, 7, 9), "6")
    MINOR_6 = ChordProperties((0, 3, 7, 9), "m6")

    # 9th extensions
    MAJOR_9 = ChordProperties((0, 4, 7, 11, 14), "maj9")
    MINOR_9 = ChordProperties((0, 3, 7, 10, 13), "m9")
    DOMINANT_9 = ChordProperties((0, 4, 7, 10, 13), "9")

    @classmethod
    def from_text(cls, name: str) -> tuple[str, "ChordType", str]:
        note_name_text, chord_type_text = CHORD_PATTERN.match(name).groups()
        args = chord_type_text.split("/")
        if len(args) == 2:
            chord_type_text, bass_note_text = args
        else:
            chord_type_text = args[0]
            bass_note_text = ""

        for chord_type in cls:
            if chord_type.value.notation == chord_type_text:
                break
        else:
            raise ValueError(f"{chord_type_text} is not a known chord name.")

        return note_name_text, chord_type, bass_note_text


def chord_name_to_sound(
    name: str,
    note_length: BaseNoteLength,
    octave: int = 0,
    **sound_kwargs,
):
    if not name:
        return Sound.rest(
            note_length=note_length,
            **sound_kwargs,
        )

    first_note_name, chord_type, bass_note_name = ChordType.from_text(name)
    sound = chord_type.as_sound(
        f"{first_note_name}{octave}",
        note_length=note_length,
        **sound_kwargs,
    )
    if bass_note_name:
        first_note = sorted(sound.notes, key=lambda x: x.freq)[0]
        if note_difference_octave(first_note.letter, bass_note_name) < 0:
            octave -= 1

        bass_note_name = f"{bass_note_name}{octave}"
        sound.notes = (*sound.notes, Note(bass_note_name))

    return sound


def scale_to_chords(scale: Scale, exts: int = 3) -> tuple[Chord]:
    s = scale.value
    return tuple(
        Chord(
            ChordProperties(
                [
                    *(s[(x * 2 + i) % len(s)] for x in range(exts)),
                ],
                notation="",
            ),
        )
        for i in range(len(s))
    )


def chord_sounds_to_bassline(sounds: list[Sound], shift_st=-24) -> list[Sound]:
    return [
        Sound.rest(note_length=s.note_length)
        if s.is_rest
        else Sound(
            notes=[s.notes[0].transpose(shift_st)],
            note_length=s.note_length,
        )
        for s in sounds
    ]


def chord_name_from_note_names(*note_names: str) -> str:
    root_note_name = Note(note_names[0]).letter
    third_interval = full_note_difference(note_names[0], note_names[1])
    fifth_interval = full_note_difference(note_names[0], note_names[2])

    match third_interval:
        case 2:
            third_spec = "sus2"
        case 3:
            third_spec = "m"
        case 4:
            third_spec = ""
        case 5:
            third_spec = "sus4"
        case _:
            raise ValueError(f"invalid third interval: {third_interval}")

    match fifth_interval:
        case 6:
            fifth_spec = "dim"
            if third_spec == "m":
                third_spec = ""
            else:
                third_spec = f"({third_spec})"
        case 7:
            fifth_spec = ""
        case 8:
            fifth_spec = "aug"
        case _:
            raise ValueError(f"invalid fifth interval: {fifth_interval}")

    return f"{root_note_name}{fifth_spec}{third_spec}"
