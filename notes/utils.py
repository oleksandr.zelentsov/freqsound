import math
from decimal import Decimal

from pickle_function_cache import cache_responses

from consts import (
    A4_FREQ,
    CACHE_PATH,
    HALF_STEP_MULTIPLIER,
    NOTE_HALF_TONE_NUMBERS,
    NOTE_PATTERN,
    OCTAVE_NOTE_PATTERN,
)


@cache_responses(CACHE_PATH)
def freq_to_note(freq: float | Decimal, a4_freq: float = A4_FREQ) -> tuple[str, int]:
    notes = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]

    note_number = 12 * math.log2(freq / a4_freq) + 49
    note_number = round(note_number)

    note = (note_number - 1) % len(notes)
    note = notes[note]

    octave = (note_number + 8) // len(notes)

    return note, octave


@cache_responses(CACHE_PATH)
def note_to_half_tone_number(note: str) -> int:
    """
    E.g. "a4" is 0, "b4" is 2
    """
    letter, modifier = OCTAVE_NOTE_PATTERN.match(note).groups()
    letter = letter.lower()
    half_tone_number = NOTE_HALF_TONE_NUMBERS[letter]
    if modifier == "#":
        half_tone_number += 1
    elif modifier == "b":
        half_tone_number -= 1

    return half_tone_number


@cache_responses(CACHE_PATH)
def note_difference_octave(note1: str, note2: str) -> int:
    """
    e.g.:
    "a#" vs "b" is 1
    "a" vs "c" is -9
    """
    htn1 = note_to_half_tone_number(note1)
    htn2 = note_to_half_tone_number(note2)
    return htn2 - htn1


@cache_responses(CACHE_PATH)
def full_note_difference(note1: str, note2: str) -> int:
    """
    e.g.:
    "a#4" vs "a#5" is 12
    "f3" vs "c4" is 7
    """
    letter1, modifier1, octave1 = NOTE_PATTERN.match(note1).groups()
    letter2, modifier2, octave2 = NOTE_PATTERN.match(note2).groups()
    letter1 = letter1.lower()
    letter2 = letter2.lower()
    octave1 = int(octave1)
    octave2 = int(octave2)

    htn = note_difference_octave(f"{letter1}{modifier1}", f"{letter2}{modifier2}")

    octaves = octave2 - octave1
    htn += 12 * octaves
    return int(htn)


@cache_responses(CACHE_PATH)
def note_name_to_frequency(
    name: str,
    half_step_multiplier: Decimal = HALF_STEP_MULTIPLIER,
    a4_freq: Decimal = A4_FREQ,
) -> Decimal:
    letter, modifier, octave = NOTE_PATTERN.match(name).groups()
    letter = letter.lower()

    # difference within octave
    htn = note_difference_octave("a", f"{letter}{modifier}")

    # octave number
    octaves = int(octave) - 4
    htn += 12 * octaves
    return transpose(
        a4_freq,
        htn,
        half_step_multiplier=half_step_multiplier,
    )


@cache_responses(CACHE_PATH)
def transpose(
    freq: Decimal,
    half_step_count: int,
    half_step_multiplier: Decimal = HALF_STEP_MULTIPLIER,
) -> Decimal:
    return freq * half_step_multiplier**half_step_count


def generate_scale(
    one_freq: Decimal,
    steps,
    half_step_multiplier=HALF_STEP_MULTIPLIER,
):
    octave = 0
    while True:
        for half_step_count in steps:
            yield transpose(
                one_freq,
                half_step_count=(octave * 12 + half_step_count),
                half_step_multiplier=half_step_multiplier,
            )

        octave += 1


# @cache_responses(CACHE_PATH)
def invert_intervals(intervals: tuple[int], inversion_num: int) -> tuple[int]:
    if inversion_num == 0:
        return intervals

    is_inversion_left = inversion_num < 0
    inversion_num = abs(inversion_num)
    for _ in range(inversion_num):
        if is_inversion_left:
            note = intervals[-1]
            intervals = intervals[:-1]
            intervals = (note - 12, *intervals)
        else:
            note = intervals[0]
            intervals = intervals[1:]
            intervals = (*intervals, note + 12)

    return intervals
