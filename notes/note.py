import math
import re
from dataclasses import dataclass, field
from decimal import Decimal
from enum import Enum

from notes.utils import (
    freq_to_note,
    full_note_difference,
    note_name_to_frequency,
    transpose,
)


class BaseNoteLength:
    def get_duration_s(self, bpm: float):
        return 4 * self.value / (bpm / 60)

    def __add__(self, other: "BaseNoteLength"):
        return CustomNoteLength(self.value + other.value)

    def __mul__(self, other: int | float):
        return CustomNoteLength(self.value * other)

    def __truediv__(self, other: int | float):
        return CustomNoteLength(self.value / other)


@dataclass(frozen=True)
class CustomNoteLength(BaseNoteLength):
    value: float


class NoteLength(BaseNoteLength, Enum):
    D1 = T1 = WHOLE = 1

    # duplets
    D2 = HALF = 1 / 2
    D4 = QUARTER = 1 / 4
    D8 = EIGHTH = 1 / 8
    D16 = SIXTEENTH = 1 / 16
    D32 = THIRTY_SECOND = 1 / 32
    D64 = SIXTY_FORTH = 1 / 64

    # triplets
    T3 = THIRD = 1 / 3
    T6 = SIXTH = 1 / 6
    T12 = TWELFTH = 1 / 12
    T24 = TWENTY_FORTH = 1 / 24
    T48 = FORTY_EIGHTH = 1 / 48

    def __repr__(self) -> str:
        return f"NoteLength.{self.name}"

    @classmethod
    def triplet_types(cls):
        return (
            cls.T3,
            cls.T6,
            cls.T12,
            cls.T24,
        )

    @classmethod
    def duplet_types(cls):
        return (
            cls.D2,
            cls.D4,
            cls.D8,
            cls.D16,
            cls.D32,
        )


NOTE_NAME_PATTERN = re.compile(r"([A-Ga-g]{1}[\#b]?)([0-9]+)")


@dataclass(frozen=True)
class Note:
    name: str | Decimal | float = None
    freq: Decimal = field(default=None, repr=False)
    volume: int = field(default=100, repr=False, hash=False, compare=False)
    custom_note_length: NoteLength | None = None

    @property
    def letter(self):
        return (NOTE_NAME_PATTERN.match(self.name).groups()[0]).upper()

    @property
    def octave(self):
        return int(NOTE_NAME_PATTERN.match(self.name).groups()[1])

    def __lt__(self, other: "Note"):
        return self.freq < other.freq

    def __le__(self, other: "Note"):
        return self.freq <= other.freq

    def __gt__(self, other: "Note"):
        return self.freq > other.freq

    def __ge__(self, other: "Note"):
        return self.freq >= other.freq

    def __post_init__(self):
        if isinstance(self.name, str):
            object.__setattr__(self, "name", self.name.lower())
            if not self.freq:
                object.__setattr__(self, "freq", note_name_to_frequency(self.name))
        elif not self.freq:
            raise ValueError("invalid note frequency or name")

    def transpose(self, semitones):  # TODO add a4 freq usage
        new_freq = transpose(self.freq, semitones)
        letter, octave = freq_to_note(new_freq)
        return Note(
            name=f"{letter}{octave}",
            freq=new_freq,
            volume=self.volume,
            custom_note_length=self.custom_note_length,
        )

    @classmethod
    def from_freq(cls, freq, **kwargs):  # TODO add a4 freq usage
        name, octave = freq_to_note(freq)
        name = f"{name}{octave}"
        return cls(name=name, freq=freq, **kwargs)

    def __eq__(self, other):
        return all(
            [
                isinstance(other, Note),
                self.name == other.name,
                math.isclose(self.freq, other.freq, rel_tol=1e-4),
                self.custom_note_length == other.custom_note_length,
            ]
        )


def note_range(note1: str | Note, note2: str | Note):
    note1 = note1 if isinstance(note1, Note) else Note(note1)
    note2 = note2 if isinstance(note2, Note) else Note(note2)

    if note1 >= note2:
        raise ValueError("notes should be provided in the correct order")

    for note_i in (
        note1.transpose(i) for i in range(full_note_difference(note1.name, note2.name))
    ):
        yield note_i
