import random
from abc import ABC, abstractmethod
from copy import deepcopy
from dataclasses import dataclass
from typing import ClassVar, Generator

from notes.note import BaseNoteLength, NoteLength


@dataclass
class BaseRhythmGenerator(ABC):
    @abstractmethod
    def generate_rhythm(self) -> Generator[BaseNoteLength, None, None]:
        ...


@dataclass
class RhythmRepeater(BaseRhythmGenerator):
    rhythm: list[BaseNoteLength]

    def generate_rhythm(self) -> Generator[BaseNoteLength, None, None]:
        while True:
            yield from deepcopy(self.rhythm)


@dataclass
class RandomMeasureSubdivision(BaseRhythmGenerator):
    minimum_note_length: BaseNoteLength = NoteLength.T6
    subdivision_count: int = 2
    repeat_times: int | None = None
    starting_note_length: BaseNoteLength = NoteLength.WHOLE
    subdivision_rate: float = 0.5

    rhythm_hierarchy: ClassVar[dict] = {
        # duplets
        NoteLength.WHOLE: (
            (NoteLength.HALF, NoteLength.HALF),
            (NoteLength.THIRD, NoteLength.THIRD, NoteLength.THIRD),
        ),
        NoteLength.HALF: (
            (NoteLength.QUARTER, NoteLength.QUARTER),
            (NoteLength.SIXTH, NoteLength.SIXTH, NoteLength.SIXTH),
        ),
        NoteLength.QUARTER: (
            (NoteLength.EIGHTH, NoteLength.EIGHTH),
            (NoteLength.TWELFTH, NoteLength.TWELFTH, NoteLength.TWELFTH),
        ),
        NoteLength.EIGHTH: (
            (NoteLength.SIXTEENTH, NoteLength.SIXTEENTH),
            (NoteLength.TWENTY_FORTH, NoteLength.TWENTY_FORTH, NoteLength.TWENTY_FORTH),
        ),
        NoteLength.SIXTEENTH: (
            (NoteLength.THIRTY_SECOND, NoteLength.THIRTY_SECOND),
            (NoteLength.FORTY_EIGHTH, NoteLength.FORTY_EIGHTH, NoteLength.FORTY_EIGHTH),
        ),
        # triplets
        NoteLength.THIRD: ((NoteLength.SIXTH, NoteLength.SIXTH),),
        NoteLength.SIXTH: ((NoteLength.TWELFTH, NoteLength.TWELFTH),),
        NoteLength.TWELFTH: ((NoteLength.TWENTY_FORTH, NoteLength.TWENTY_FORTH),),
        NoteLength.TWENTY_FORTH: ((NoteLength.FORTY_EIGHTH, NoteLength.FORTY_EIGHTH),),
    }

    def subdivide(self, note_length: BaseNoteLength) -> tuple[BaseNoteLength]:
        if note_length.value <= self.minimum_note_length.value:
            return (note_length,)  # do not subdivide the note

        if (
            note_length != self.starting_note_length
            and random.random() >= self.subdivision_rate
        ):
            return (note_length,)  # about 50% of notes will not be subdivided

        possible_subdivisions = self.rhythm_hierarchy.get(note_length)
        if not possible_subdivisions:
            return (note_length,)
        result = random.choice(possible_subdivisions)
        for _ in range(self.subdivision_count):
            for note_length in result:
                subdivision = self.subdivide(note_length)
                result = (
                    (*result, *subdivision) if subdivision else (*result, note_length)
                )

        return result

    def generate_rhythm(self) -> Generator[BaseNoteLength, None, None]:
        if self.repeat_times:
            subdivided = [
                self.subdivide(self.starting_note_length)
                for _ in range(self.repeat_times)
            ]
            while True:
                for x in subdivided:
                    yield from x
        else:
            while True:
                yield from self.subdivide(self.starting_note_length)


@dataclass
class ShiftedRhythmGenerator(BaseRhythmGenerator):
    rhythm_gen: BaseRhythmGenerator
    shift: NoteLength

    def generate_rhythm(self) -> Generator[BaseNoteLength, None, None]:
        yield from (r + self.shift for r in self.rhythm_gen.generate_rhythm())
