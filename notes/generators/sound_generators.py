import random
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from itertools import chain, islice, repeat
from typing import Any, Generator

from notes.chords import Chord, chord_name_to_sound, scale_to_chords
from notes.generators.note_generators import BaseNoteGenerator
from notes.generators.rhythm_generators import BaseRhythmGenerator
from notes.note import Note
from notes.scales import Scale
from notes.sound import Sound


class SoundGenerator(ABC):
    @abstractmethod
    def generate_infinite_sound(self) -> Generator[Sound, None, None]:
        ...

    def generate_sounds(
        self,
        audio_length: int | float | None = None,
        note_count: int = None,
        measure_count: int = None,
        bpm: float | int | None = None,
    ) -> Generator[Sound, None, None]:
        gen = self.generate_infinite_sound()
        if audio_length is not None:
            while audio_length > 0:
                new_sound = next(gen)
                yield new_sound
                new_sound_time = new_sound.note_length.get_duration_s(bpm)
                audio_length -= new_sound_time
        elif measure_count is not None:
            while measure_count > 0:
                new_sound = next(gen)
                yield new_sound
                new_sound_measure_count = new_sound.note_length.value
                measure_count -= new_sound_measure_count
        elif note_count is not None:
            yield from islice(gen, note_count)
        else:
            yield from gen


@dataclass
class SimpleSingleRandomSoundGenerator(SoundGenerator):
    note_generator: BaseNoteGenerator
    rhythm_generator: BaseRhythmGenerator

    def generate_infinite_sound(self):
        note_gen = self.note_generator.generate_scale()
        rhythm_gen = self.rhythm_generator.generate_rhythm()
        while True:
            if note := next(note_gen):
                yield Sound([note], note_length=next(rhythm_gen))
            else:
                yield Sound.rest(next(rhythm_gen))


@dataclass
class ScaleDegreesChordsSoundGenerator(
    SoundGenerator
):  # TODO refactor both into note gens
    root: Note
    scale: Scale
    rhythm_generator: BaseRhythmGenerator
    degrees: list[int]

    _chords: tuple[Chord] = None

    def __post_init__(self):
        self._chords = scale_to_chords(self.scale)

    def generate_infinite_sound(self) -> Generator[Sound, None, None]:
        rg = self.rhythm_generator.generate_rhythm()
        for degrees in chain(repeat(self.degrees)):
            for degree in degrees:
                if not degree:
                    yield Sound.rest(note_length=nl)
                    continue

                nl = next(rg)
                chord = self._chords[(degree - 1) % len(self._chords)]
                yield chord.as_sound(
                    root=self.root.freq,
                    note_length=nl,
                )


@dataclass
class ScaleChordsSoundGenerator(SoundGenerator):
    root: Note
    scale: Scale
    rhythm_generator: BaseRhythmGenerator

    _chords: tuple[Chord] = None

    def __post_init__(self):
        self._chords = scale_to_chords(self.scale)

    def generate_infinite_sound(self) -> Generator[Sound, None, None]:
        rg = self.rhythm_generator.generate_rhythm()
        while True:
            nl = next(rg)
            chord = random.choice(self._chords)
            yield chord.as_sound(
                root=self.root.freq,
                note_length=nl,
            )


@dataclass
class ChordProbability:
    to_chord: str
    probability: float = 0
    chord_sound_kwargs: dict[str, Any] = field(default_factory=dict)


@dataclass
class TreeChordSoundGenerator(SoundGenerator):
    first_chord: str
    tree: dict[str, tuple[ChordProbability]]
    rhythm_generator: BaseRhythmGenerator
    octave: int = 4
    sound_kwargs: dict = field(default_factory=dict)
    repeat_after: int | None = None

    def __post_init__(self):
        for _, probabilities in self.tree.items():
            for prob in probabilities:
                destination_number = len(probabilities)
                if prob.probability == 0:
                    prob.probability = 1 / destination_number

    def get_next_chord(self, current_chord: str) -> ChordProbability:
        destinations = self.tree.get(current_chord, list(self.tree.values())[0])
        if len(destinations) == 1:
            return destinations[0]

        return random.choices(
            destinations,
            weights=[d.probability for d in destinations],
        )[0]

    def generate_repeat_after(self, rhythm_gen) -> Generator[Sound, None, None]:
        current_chord = self.first_chord
        sound = chord_name_to_sound(
            current_chord,
            note_length=next(rhythm_gen),
            octave=self.octave,
            **self.sound_kwargs,
        )
        sounds = [sound]
        yield sound
        while True:
            if len(sounds) == self.repeat_after:
                yield from sounds
                continue

            probability = self.get_next_chord(current_chord)
            current_chord = probability.to_chord
            sound_kwargs = {**self.sound_kwargs, **probability.chord_sound_kwargs}
            sound = chord_name_to_sound(
                current_chord,
                note_length=next(rhythm_gen),
                octave=self.octave,
                **sound_kwargs,
            )
            sounds.append(sound)

    def generate_no_repeat(self, rhythm_gen) -> Generator[Sound, None, None]:
        current_chord = self.first_chord
        yield chord_name_to_sound(
            current_chord,
            note_length=next(rhythm_gen),
            octave=self.octave,
            **self.sound_kwargs,
        )
        while True:
            probability = self.get_next_chord(current_chord)
            current_chord = probability.to_chord
            sound_kwargs = {**self.sound_kwargs, **probability.chord_sound_kwargs}
            yield chord_name_to_sound(
                current_chord,
                note_length=next(rhythm_gen),
                octave=self.octave,
                **sound_kwargs,
            )

    def generate_infinite_sound(self) -> Generator[Sound, None, None]:
        rhythm_gen = self.rhythm_generator.generate_rhythm()
        if self.repeat_after:
            yield from self.generate_repeat_after(rhythm_gen)
        else:
            yield from self.generate_no_repeat(rhythm_gen)
