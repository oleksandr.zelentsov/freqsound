from abc import ABC, abstractmethod
from copy import deepcopy
from dataclasses import dataclass, field
from functools import partial
from random import randint, sample
from typing import ClassVar, Generator

from notes.generators.sound_generators import SoundGenerator
from notes.note import Note, NoteLength
from notes.scales import BaseScale
from notes.sound import Note, Sound


class BasePatternSwitcher(ABC):
    @abstractmethod
    def __call__(self, patterns) -> Generator["Pattern", None, None]:
        ...


class ABPatternSwitcher(BasePatternSwitcher):
    def __call__(self, patterns):
        while True:
            yield from patterns


@dataclass
class RandomizedShiftPatternSwitcher(BasePatternSwitcher):
    shift_range: tuple[int, int]

    def __call__(self, patterns):
        while True:
            t_patterns: list[Pattern] = sample(patterns, len(patterns))
            yield from (
                pattern.shifted_variation(randint(*self.shift_range))
                for pattern in t_patterns
            )


@dataclass
class Pattern(SoundGenerator):
    scale_degree_sequence: list[int]
    note_lengths: list[NoteLength]
    starting_note: Note
    scale: BaseScale
    scale_shift: int = 0

    @property
    def measure_count(self) -> float:
        return sum(note_length.value for note_length in self.note_lengths)

    def shifted_variation(self, shift) -> "Pattern":
        result = deepcopy(self)
        result.scale_shift = shift
        return result

    def generate_infinite_sound(self) -> Generator[Sound, None, None]:
        note_sequence = [
            self.starting_note.transpose(interval) if interval is not None else None
            for interval in [
                self.scale[scale_degree - 1 + self.scale_shift]
                if scale_degree is not None
                else None
                for scale_degree in self.scale_degree_sequence
            ]
        ]
        length_sequence = self.note_lengths
        sounds = [
            Sound([note], length) if note is not None else Sound.rest(length)
            for note, length in zip(note_sequence, length_sequence)
        ]
        while True:
            yield from sounds

    @classmethod
    def make_template(cls, *args, **kwargs):
        return partial(cls, *args, **kwargs)


# @dataclass
# class ShiftSwitcherPattern(Pattern):
#     shifts: tuple[int, int] = (-1, 0, 2, 1, 3)
#     direction: PassType = PassType.UP_DOWN

#     def generate_infinite_sound(self) -> Generator[Sound, None, None]:
#         len_scale = len(self.scale)
#         shift_idx = 0
#         while True:
#             yield from [
#                 self.starting_note.transpose(interval)
#                 for interval in [
#                     self.scale.value[(scale_degree + self.scale_shift) % len_scale]
#                     for scale_degree in self.scale_degree_sequence
#                 ]
#             ]
#             shift_idx = (shift_idx + 1) % len(self.shifts)
#             self.scale_shift = self.shifts[shift_idx]


@dataclass
class PatternedSoundGenerator(SoundGenerator):
    starting_note: Note
    scale: BaseScale
    pattern_templates: list[type] = field(default_factory=list)
    pattern_switcher: BasePatternSwitcher = ABPatternSwitcher()
    default_pattern_templates: ClassVar[list[Pattern | type]] = [
        Pattern.make_template(
            [3, 2, 1],
            [
                NoteLength.T6,
                NoteLength.T6,
                NoteLength.T6,
            ],
        ),
        Pattern.make_template(
            [3, 2, 1, 4, 3, 2],
            [
                NoteLength.D16,
                NoteLength.D16,
                NoteLength.D8,
                NoteLength.D16,
                NoteLength.D16,
                NoteLength.D8,
            ],
        ),
        Pattern.make_template(
            [3, 1, 2],
            [
                NoteLength.D4,
                NoteLength.D4,
                NoteLength.D2,
            ],
        ),
        Pattern.make_template(
            [0, None, 5, 5, 4],
            [
                NoteLength.D2,
                NoteLength.D8,
                NoteLength.D8,
                NoteLength.D4,
                NoteLength.D1,
            ],
        ),
    ]
    use_default_patterns: bool = False

    patterns: list[Pattern] = field(default=None)

    def get_pattern_kwargs(self):
        return {
            "starting_note": self.starting_note,
            "scale": self.scale,
        }

    def __post_init__(self):
        if not self.patterns:
            pattern_kwargs = self.get_pattern_kwargs()
            def_pats = (
                self.default_pattern_templates if self.use_default_patterns else []
            )
            self.patterns = [
                pat_type(**pattern_kwargs)
                for pat_type in def_pats + self.pattern_templates
            ]

    def generate_infinite_sound(self) -> Generator[Sound, None, None]:
        for pattern in self.pattern_switcher(self.patterns):
            yield from pattern.generate_sounds(note_count=len(pattern.note_lengths))
