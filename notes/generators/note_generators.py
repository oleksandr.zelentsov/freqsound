import operator
import random
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from enum import Enum
from functools import partial, reduce
from typing import Generator

from consts import A4_FREQ
from notes.scales import BaseScale
from notes.sound import Note


@dataclass
class BaseNoteGenerator(ABC):
    @abstractmethod
    def generate_scale(self) -> Generator[Note | None, None, None]:
        ...


class PassType(Enum):
    UP = "up"
    DOWN = "down"
    UP_DOWN = "up_down"


@dataclass
class UpDownScaleNoteGenerator(BaseNoteGenerator):
    scale: BaseScale
    beginning_note: Note = field(default_factory=partial(Note, freq=A4_FREQ))
    pass_type: PassType = PassType.UP
    more_octaves: int = 0

    def generate_scale(self) -> Generator[Note | None, None, None]:
        old_sound = self.beginning_note
        if self.pass_type == PassType.UP:
            scale = list(
                reduce(
                    operator.add,
                    [
                        [x + 12 * (oct) for x in self.scale.value]
                        for oct in range(1, self.more_octaves + 1)
                    ],
                    self.scale.value,
                )
            )
        elif self.pass_type == PassType.DOWN:
            scale = list(
                reduce(
                    operator.add,
                    [
                        [x + 12 * (oct) for x in self.scale.value[::-1]]
                        for oct in range(1, self.more_octaves + 1)
                    ],
                    self.scale.value[::-1],
                )
            )
        elif self.more_octaves:
            scale = list(
                reduce(
                    operator.add,
                    [
                        [x + 12 * (oct) for x in self.scale.value]
                        for oct in range(1, self.more_octaves + 1)
                    ],
                    self.scale.value,
                )
            )
            scale += list(
                reduce(
                    operator.add,
                    [
                        [x + 12 * (oct) for x in self.scale.value[::-1][1:]]
                        for oct in range(self.more_octaves, -1, -1)
                    ],
                )
            )
        else:
            scale = self.scale.value + self.scale.value[::-1][1:]

        while True:
            yield from (old_sound.transpose(x) for x in scale)


def _check_interval(
    max_scale_step: int,
    i: int,
    old_interval_index: int,
) -> bool:
    return (
        (old_interval_index - max_scale_step)
        <= i
        <= (old_interval_index + max_scale_step)
    )


@dataclass
class RandomScaleOctaveNoteGenerator(BaseNoteGenerator):
    scale: BaseScale
    beginning_note: Note = field(default_factory=partial(Note, freq=A4_FREQ))
    min_octave_randomness: int = 0
    max_octave_randomness: int = 1
    note_silence_probability: float = 0.1
    max_scale_step: int = 12
    octave_jump_probability: float = 0.08

    def generate_scale(self) -> Generator[Note | None, None, None]:
        old_sound = self.beginning_note
        available_intervals = self.scale.value
        old_chosen_inteval = 0
        while True:
            av_ints = [
                interval
                for i, interval in enumerate(available_intervals)
                if _check_interval(self.max_scale_step, i, old_chosen_inteval)
            ]
            step = random.choice(av_ints)
            is_step_silent = random.random() < self.note_silence_probability
            if is_step_silent:
                yield None

            is_octave_jump = random.random() < self.octave_jump_probability
            if is_octave_jump:
                octave = random.randint(
                    self.min_octave_randomness,
                    self.max_octave_randomness,
                )
                is_octave_right = random.choice([-1, 1])
            else:
                octave = 0
                is_octave_right = 1

            yield old_sound.transpose(step + is_octave_right * 12 * octave)
            old_chosen_inteval = available_intervals.index(step)
