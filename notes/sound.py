from dataclasses import dataclass
from itertools import chain

from notes.note import BaseNoteLength, Note
from notes.utils import transpose


@dataclass
class Sound:
    notes: tuple[Note]
    note_length: BaseNoteLength

    @property
    def total_note_length(self) -> BaseNoteLength:
        return max(chain([self.note_length], map(lambda x: x.note_length, self.notes)))

    @property
    def is_rest(self):
        return not bool(self.notes)

    def transpose(self, transpose_num: int):
        return Sound(
            [
                Note(
                    freq=transpose(note.freq, transpose_num),
                    volume=note.volume,
                    custom_note_length=note.custom_note_length,
                )
                for note in self.notes
                if note
            ],
            note_length=self.note_length,
        )

    def change_voicing(self, note_to_octave: list[int]):
        notes = [
            note.transpose(12 * octave)
            for note, octave in zip(self.notes, note_to_octave)
            if octave != 0
        ]
        if len(notes) < len(self.notes):
            notes.extend(self.notes[len(notes) - 1 :])

        return Sound(
            notes,
            self.note_length,
        )

    @classmethod
    def sounds_to_measure_count(cls, sounds: list["Sound"]):
        return sum(sound.note_length.value for sound in sounds)

    @classmethod
    def rest(cls, note_length: BaseNoteLength):
        return cls(
            [],
            note_length=note_length,
        )
