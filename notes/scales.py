from copy import deepcopy
from dataclasses import dataclass, field
from enum import Enum


class ScaleMode(Enum):
    IONIAN = 1
    DORIAN = 2
    PHRYGIAN = 3
    LYDIAN = 4
    MIXOLYDIAN = 5
    AEOLIAN = 6
    LOCRIAN = 7


def sign(x: float):
    return -1 if x < 0 else 1


class BaseScale:
    @property
    def root(self):
        return self.value[0]

    def lengthen(self, n: int):
        if n == 0:
            return self
        elif n < 0:
            raise ValueError("cannot lengthen negatively")

        new_value = deepcopy(self.value)
        val_len = len(new_value)
        for i in range(n):
            orig_interval_position = i % val_len
            orig_interval = self.value[orig_interval_position]
            new_value.append(12 * ((i // val_len) + 1) + orig_interval)

        return Scale(new_value)

    def shorten(self, n: int):
        if n == 0:
            return self
        elif n < 0:
            raise ValueError("cannot shorten negatively")
        elif n == len(self.value):
            raise ValueError("cannot shorten scale down to 0 elements")

        return Scale(self.value[:-n])

    def use_mode(self, mode: ScaleMode):
        mode_value = mode.value
        first_element_index = mode_value - 1
        new_value = self.value[first_element_index:] + [
            12 + x for x in self.value[:first_element_index]
        ]
        return Scale(new_value)

    def reverse(self) -> "BaseScale":
        return Scale([0] + [12 - el for el in self.value[::-1][:-1]])

    def __getitem__(self, key):
        key -= 1
        sign = -1 if key < 0 else 1
        key = abs(key)
        scale_len = len(self.value)
        octave_count, basic_scale_degree = divmod(key, scale_len)
        if sign > 0:
            return (octave_count * 12) + self.value[basic_scale_degree]

        reverse_scale = self.reverse()
        return -(octave_count * 12) - reverse_scale.value[basic_scale_degree]


@dataclass
class Scale(BaseScale):
    """Represents any kind of scale."""

    value: list[int] = field(default_factory=[])


class ScaleType(BaseScale, Enum):
    """Has a few shortcuts for widely used scales"""

    MAJOR = [0, 2, 4, 5, 7, 9, 11]
    HARMONIC_MAJOR = [0, 2, 4, 5, 7, 8, 11]
    MINOR = NATURAL_MINOR = [0, 2, 3, 5, 7, 8, 11]
    HARMONIC_MINOR = [0, 2, 3, 5, 7, 10, 11]
    DIMINISHED = [0, 2, 3, 5, 6, 8, 9, 11]

    # MINOR_PENTATONIC = [0, 2, 3, 5, 7, 8, 10]
