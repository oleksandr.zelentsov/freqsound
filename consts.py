import os
import re
from decimal import Decimal

# general
CACHE_PATH = os.path.expanduser("~/.freqsound-cache")

# notes
HALF_STEP_MULTIPLIER = Decimal("2") ** (Decimal("1") / Decimal("12"))
MAJOR_SCALE_INTERVALS = (2, 2, 1, 2, 2, 2, 1)
SAMPLE_RATE = int(os.getenv("SAMPLE_RATE") or "48000")
A4_FREQ = Decimal(os.getenv("A4_FREQ") or "440")
BIT_DEPTH = int(os.getenv("BIT_DEPTH") or 16)
NOTE_PATTERN = re.compile(r"([a-gA-G])([\#b]?)(\-?[0-9]+)")
OCTAVE_NOTE_PATTERN = re.compile(r"([a-gA-G])([\#b]?)")
NOTES = "cdefgab"
NOTE_HALF_TONE_NUMBERS = {
    note: sum((0, *MAJOR_SCALE_INTERVALS[:i])) for i, note in enumerate(NOTES)
}

# examples
DEFAULT_MAIN_LOCATION = "examples.songs.where_is_my_mind.main"
MAIN_LOCATION = os.getenv("MAIN_INPUT") or DEFAULT_MAIN_LOCATION
MAIN_OUTPUT = os.getenv("MAIN_OUTPUT") or "out.wav"
