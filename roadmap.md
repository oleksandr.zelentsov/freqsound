# Future features

- stereo
- midi input into library to generate sound using
- varying velocity and pan in generated music
- different ways to build chords from scales
- pattern-based melody generation

# Ideas
- auto-compose with a rhythm repeater that will define song structure (e.g. 8 measures solo, 4 measures chorus); this should result with call and response like feel
