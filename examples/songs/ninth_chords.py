from examples.consts import SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE
from notes.chords import ChordType
from notes.note import NoteLength
from notes.sound import Sound
from waves.songs import Song, Track

CHORDS = [
    ChordType.MINOR_9.as_sound("c4", NoteLength.D2, fall_volume=85),
    Sound.rest(NoteLength.D2),
    ChordType.DOMINANT_9.as_sound("c4", NoteLength.D2, fall_volume=85),
    Sound.rest(NoteLength.D2),
    ChordType.MAJOR_9.as_sound("c4", NoteLength.D2, fall_volume=85),
    Sound.rest(NoteLength.D2),
]


def main():
    return Song(
        [
            Track(FILTERED_SQUARE, CHORDS),
        ],
        **(SOUND_KWARGS | {"repeat": 1}),
    )
