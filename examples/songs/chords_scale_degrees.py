from functools import partial

from examples.consts import BPM
from examples.wave_gens import FILTERED_SQUARE, FILTERED_SQUARE_BASS
from notes.chords import chord_sounds_to_bassline
from notes.generators.rhythm_generators import RhythmRepeater
from notes.generators.sound_generators import ScaleDegreesChordsSoundGenerator
from notes.note import Note, NoteLength
from notes.scales import ScaleType
from waves.manipulation import apply_volume
from waves.songs import Song, Track


def main():
    chord_gen = ScaleDegreesChordsSoundGenerator(
        root=Note("g3"),
        # scale=ScaleType.NATURAL_MINOR,
        scale=ScaleType.MAJOR,
        rhythm_generator=RhythmRepeater(
            [
                NoteLength.D2,
                NoteLength.D2,
                NoteLength.D2,
                NoteLength.D2,
                NoteLength.D4,
                NoteLength.D4,
            ]
        ),
        degrees=[1, 1, 7, 6, 6, 4, 5],
    )
    chords = list(chord_gen.generate_sounds(audio_length=10, bpm=BPM))
    return Song(
        [
            Track(
                FILTERED_SQUARE,
                chords,
                [partial(apply_volume, volume=40)],
            ),
            Track(
                FILTERED_SQUARE_BASS,
                chord_sounds_to_bassline(chords),
            ),
        ],
        transpose=2,
        bpm=BPM,
    )
