from functools import partial

from examples.consts import BPM, SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE
from notes.generators.note_generators import PassType, UpDownScaleNoteGenerator
from notes.generators.rhythm_generators import RhythmRepeater
from notes.generators.sound_generators import SimpleSingleRandomSoundGenerator
from notes.note import Note, NoteLength
from notes.scales import ScaleType
from waves.manipulation import apply_volume
from waves.songs import Song, SoundMetronome, Track

COMPLEX_RHYTHM_GEN = RhythmRepeater(
    [
        NoteLength.D4 + NoteLength.D8,
        NoteLength.D8,
        NoteLength.T6 * 3,
    ]
)

COMPLEX_RHYTHM_SOUND_GEN = SimpleSingleRandomSoundGenerator(
    UpDownScaleNoteGenerator(
        scale=ScaleType.NATURAL_MINOR.lengthen(1),
        beginning_note=Note("f4"),
        pass_type=PassType.UP_DOWN,
    ),
    COMPLEX_RHYTHM_GEN,
)


def main():
    notes = list(COMPLEX_RHYTHM_SOUND_GEN.generate_sounds(audio_length=10, bpm=BPM))
    wave = Song(
        [
            Track(
                FILTERED_SQUARE,
                notes,
                pipeline=[
                    partial(apply_volume, volume=50),
                ],
            ),
            SoundMetronome(sounds=notes, volume=20),
        ],
        **(SOUND_KWARGS | {"repeat": 1}),
    )
    return wave
