import random
from functools import partial

from examples.consts import SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE, FILTERED_SQUARE_BASS, SQUARE
from freeze_random_state import freeze_random_seed
from notes.chords import chord_sounds_to_bassline
from notes.generators.note_generators import RandomScaleOctaveNoteGenerator
from notes.generators.rhythm_generators import RandomMeasureSubdivision
from notes.generators.sound_generators import (
    ScaleChordsSoundGenerator,
    SimpleSingleRandomSoundGenerator,
)
from notes.note import NoteLength, note_range
from notes.scales import ScaleType
from waves.manipulation import apply_volume
from waves.songs import Song, Track


@freeze_random_seed(0x17C176F36)
def main():
    BPM = random.randint(75, 150)
    root = random.choice(list(note_range("c4", "f#6")))
    scale = random.choice(list(ScaleType))
    melody_generator = SimpleSingleRandomSoundGenerator(
        note_generator=RandomScaleOctaveNoteGenerator(
            scale=scale.lengthen(1),
            beginning_note=root,
            note_silence_probability=0.5,
            max_scale_step=2,
        ),
        rhythm_generator=RandomMeasureSubdivision(
            starting_note_length=NoteLength.D2,
            minimum_note_length=NoteLength.T12,
            subdivision_count=3,
        ),
    )
    chords_generator = ScaleChordsSoundGenerator(
        root=root.transpose(-12),
        rhythm_generator=RandomMeasureSubdivision(
            starting_note_length=NoteLength.D1,
            minimum_note_length=NoteLength.D2,
            subdivision_count=2,
            subdivision_rate=1 / 3,
        ),
        scale=scale,
    )

    notes = list(melody_generator.generate_sounds(measure_count=8, bpm=BPM))
    chords = list(chords_generator.generate_sounds(measure_count=4, bpm=BPM)) * 2
    bass_notes = chord_sounds_to_bassline(chords)
    return Song(
        [
            Track(
                SQUARE,
                notes,
                pipeline=[
                    partial(apply_volume, volume=25),
                ],
            ),
            Track(
                FILTERED_SQUARE,
                chords,
                pipeline=[
                    partial(apply_volume, volume=60),
                ],
            ),
            Track(
                FILTERED_SQUARE_BASS,
                bass_notes,
                pipeline=[
                    partial(apply_volume, volume=120),
                ],
            ),
        ],
        **(SOUND_KWARGS | {"repeat": 1, "bpm": BPM}),
    )
