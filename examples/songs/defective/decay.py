from examples.consts import SOUND_KWARGS
from examples.sound_gens import NOTE_SCALE_GEN
from examples.wave_gens import DECAYED_SINE
from waves.songs import Song, SoundMetronome, Track

BPM = 103


def main():
    sounds = NOTE_SCALE_GEN.generate_sounds(audio_length=4, bpm=BPM)
    return Song(
        [
            Track(DECAYED_SINE, sounds),
            SoundMetronome(sounds=sounds),
        ],
        **(SOUND_KWARGS | {"bpm": BPM}),
    )
