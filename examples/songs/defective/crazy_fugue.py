import pytest
from auto_composers.v1_fugue_trio import main as main_fugue
from freeze_random_state import freeze_random_seed


@pytest.mark.xfail(reason='doesnt work')
@freeze_random_seed(0x1FC5580F0)
def main():
    return main_fugue()
