from examples.consts import SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE, SINE
from notes.chords import ChordType
from notes.note import NoteLength
from waves.generators import OvertoneMachine
from waves.songs import Song, Track

OVERTONE_SINE = OvertoneMachine(
    SINE,
    {
        1: 100,
        # 2: 60,
        3: 10,
        5: 5,
        # 7: 1,
    },
)
OVERTONE_SAW = OvertoneMachine(
    FILTERED_SQUARE,
    {
        1: 100,
        2: 60,
        3: 10,
        5: 5,
        7: 1,
    },
)

SOUNDS = [
    ChordType.MAJOR.as_sound(
        "a4",
        NoteLength.D2,
    ),
    ChordType.MAJOR.as_sound(
        "b4",
        NoteLength.D2,
    ),
    ChordType.MINOR.as_sound(
        "f#4",
        NoteLength.D2,
    ),
    ChordType.MAJOR.as_sound(
        "g4",
        NoteLength.D2,
    ),
]


def main():
    return Song(
        [
            Track(OVERTONE_SINE, SOUNDS),
        ],
        **SOUND_KWARGS,
    )
