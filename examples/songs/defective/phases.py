import numpy as np

from examples.consts import SOUND_KWARGS
from examples.wave_gens import SINE
from notes.chords import ChordType
from notes.note import NoteLength
from waves.generators import DetunedWaveGenerator, PhaseSetting
from waves.manipulation import merge_waves
from waves.songs import Song, Track

N = 6
BASE_WAVE_GEN = SINE
MIN_PHASE = 0
MAX_PHASE = 100
wave_gen = DetunedWaveGenerator(
    [
        PhaseSetting(x, v)
        for x, v in zip(
            np.linspace(MIN_PHASE, MAX_PHASE, N),
            np.linspace(100, 70, N),
            # np.repeat([100], N),
        )
    ],
    BASE_WAVE_GEN,
)


ONE_CHORD = [
    ChordType.MAJOR.as_sound(
        "a4",
        NoteLength.D1,
    ),
]


def main():
    waves = [
        Song(
            [
                Track(wave_gen, ONE_CHORD),
            ],
            **SOUND_KWARGS,
        ),
        Song(
            [
                Track(BASE_WAVE_GEN, ONE_CHORD),
            ],
            **SOUND_KWARGS,
        ),
    ]
    wave = merge_waves(*waves)
    return wave
