import numpy as np
import plotly.graph_objects as go
from scipy.signal import butter, filtfilt


def butter_lowpass_filter(data, cutoff, fs, order):
    normal_cutoff = cutoff / (fs / 2)
    # Get the filter coefficients
    b, a = butter(order, normal_cutoff, btype="low", analog=False)
    y = filtfilt(b, a, data)
    return y


def main():
    # Filter requirements.
    T = 5.0  # Sample Period
    fs = 30.0  # sample rate, Hz
    cutoff = 2  # desired cutoff frequency of the filter, Hz ,      slightly higher than actual 1.2 Hz
    order = 2  # sin wave can be approx represented as quadratic
    n = int(T * fs)  # total number of samples

    t = np.arange(initial_value, n, step)
    # sin wave
    sig = np.sin(1.2 * 2 * np.pi * t)
    # Lets add some noise
    noise = 1.5 * np.cos(9 * 2 * np.pi * t) + 0.5 * np.sin(12.0 * 2 * np.pi * t)
    data = sig + noise

    y = butter_lowpass_filter(data, cutoff, fs, order)

    fig = go.Figure()
    fig.add_trace(
        go.Scatter(y=data, line=dict(shape="spline"), name="signal with noise")
    )
    fig.add_trace(go.Scatter(y=y, line=dict(shape="spline"), name="filtered signal"))
    fig.show()
