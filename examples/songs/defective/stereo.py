from notes.chords import ChordType
from notes.note import NoteLength
from notes.sound import Sound
from waves.generators import SineWaveGenerator, StereoWaveGenerator
from waves.songs import Song, Track

swgs = [
    StereoWaveGenerator(SineWaveGenerator(phase=x), SineWaveGenerator())
    for x in [0, 10, 50, 100, 500, 1000]
]
tracks = [
    Track(
        swg,
        [Sound.rest(NoteLength.D1)] * i
        + [ChordType.MAJOR_9.as_sound("F#4", NoteLength.D1)],
    )
    for i, swg in enumerate(swgs)
]


def main():
    return Song(
        tracks,
        bpm=50,
    )
