from examples.consts import SOUND_KWARGS
from examples.wave_gens import SINE, SQUARE
from notes.note import Note, NoteLength
from notes.sound import Sound
from waves.songs import Song, Track

TEST_NOTE = [
    Sound(
        [
            Note(
                "g#4",
                volume=10,
            )
        ],
        NoteLength.HALF,
    ),
]


def main():
    return Song(
        [
            Track(SQUARE, TEST_NOTE),
            Track(SINE, [Sound.rest(NoteLength.HALF * 1.5)] + TEST_NOTE),
        ],
        **SOUND_KWARGS,
    )
