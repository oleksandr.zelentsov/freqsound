from functools import partial

from examples.consts import BPM, SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE
from freeze_random_state import freeze_random_seed
from notes.generators.note_generators import RandomScaleOctaveNoteGenerator
from notes.generators.rhythm_generators import RhythmRepeater
from notes.generators.sound_generators import SimpleSingleRandomSoundGenerator
from notes.note import Note, NoteLength
from notes.scales import ScaleType
from waves.manipulation import apply_volume
from waves.songs import Song, Track

CRAZY_RHYTHM = [
    NoteLength.D4,
    NoteLength.D8,
    NoteLength.D8,
    NoteLength.T12,
    NoteLength.T12,
    NoteLength.T12,
    NoteLength.D8,
    NoteLength.D8,
]
CRAZY_RANDOM_GEN = SimpleSingleRandomSoundGenerator(
    RandomScaleOctaveNoteGenerator(
        scale=ScaleType.NATURAL_MINOR.lengthen(1),
        beginning_note=Note("g#2"),
        note_silence_probability=0.4,
    ),
    RhythmRepeater(CRAZY_RHYTHM),
)


@freeze_random_seed(0x115792AE0)
def main():
    notes = list(CRAZY_RANDOM_GEN.generate_sounds(measure_count=8, bpm=BPM))
    return Song(
        [
            Track(
                FILTERED_SQUARE,
                notes,
                pipeline=[
                    partial(apply_volume, volume=50),
                ],
            ),
        ],
        **(SOUND_KWARGS | {"repeat": 1}),
    )
