from functools import partial

from examples.consts import BPM, SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE
from notes.generators.note_generators import PassType, UpDownScaleNoteGenerator
from notes.generators.rhythm_generators import RhythmRepeater
from notes.generators.sound_generators import SimpleSingleRandomSoundGenerator
from notes.note import Note, NoteLength
from notes.scales import ScaleMode, ScaleType
from waves.manipulation import apply_volume
from waves.songs import Song, Track

BPM = 250


def main():
    notess = []
    for mode in ScaleMode:
        melody_sound_gen = SimpleSingleRandomSoundGenerator(
            UpDownScaleNoteGenerator(
                scale=ScaleType.MAJOR.use_mode(mode).lengthen(1),
                beginning_note=Note("db4"),
                pass_type=PassType.UP,
            ),
            RhythmRepeater(
                [
                    NoteLength.D4,
                ]
            ),
        )

        notes = list(
            melody_sound_gen.generate_sounds(  # generate 8 note scale
                note_count=8,
                bpm=BPM,
            )
        )
        notess.extend(notes)

    return Song(
        [
            Track(
                FILTERED_SQUARE,
                sounds=notess,
                pipeline=[
                    partial(apply_volume, volume=50),
                ],
            )
        ],
        **{**SOUND_KWARGS, "bpm": BPM},
    )
