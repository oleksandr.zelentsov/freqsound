from examples.wave_gens import SINE
from notes.note import Note, NoteLength
from notes.scales import ScaleType
from notes.sound import Sound
from waves.songs import Song, Track


def main():
    root = Note("C4")
    sounds = []

    for i in range(-13, 3):
        transposed_note = root.transpose(ScaleType.MAJOR[i])
        sounds.append(
            Sound(
                [transposed_note],
                note_length=NoteLength.T24,
            )
        )
    return Song(
        [
            Track(
                SINE,
                sounds,
            )
        ]
    )
