from examples.consts import BPM, SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE
from freeze_random_state import freeze_random_seed
from notes.generators.rhythm_generators import RhythmRepeater
from notes.generators.sound_generators import ChordProbability, TreeChordSoundGenerator
from notes.note import NoteLength
from waves.songs import Song, Track

TREE_CHORD_GENERATOR = TreeChordSoundGenerator(
    "Cm",
    tree={
        "Cm": [
            ChordProbability("Fm7", chord_sound_kwargs={"inversion": -1}),
            ChordProbability("G", chord_sound_kwargs={"inversion": -1}),
        ],
        "G": [
            ChordProbability("Cm"),
            ChordProbability("G#maj7", chord_sound_kwargs={"inversion": -1}),
            ChordProbability(""),
        ],
        "Fm7": [
            ChordProbability("Cm"),
            ChordProbability("G", chord_sound_kwargs={"inversion": -1}),
            ChordProbability(""),
        ],
        "G#maj7": [
            ChordProbability("Fm7", chord_sound_kwargs={"inversion": -1}),
            ChordProbability("Cm"),
            ChordProbability("A#7", chord_sound_kwargs={"inversion": -2}),
            ChordProbability(""),
        ],
        "A#7": [
            ChordProbability("G", chord_sound_kwargs={"inversion": -1}),
        ],
        "": [
            ChordProbability("Fm7", chord_sound_kwargs={"inversion": -1}),
            ChordProbability("G"),
        ],
    },
    rhythm_generator=RhythmRepeater(
        [
            NoteLength.D4,
            NoteLength.D4,
            NoteLength.D2,
            NoteLength.D4,
            NoteLength.D4,
            NoteLength.D2,
            NoteLength.D2,
            NoteLength.D2,
        ]
    ),
    repeat_after=8,
    octave=4,
)


@freeze_random_seed(1)
def main():
    notes = list(
        TREE_CHORD_GENERATOR.generate_sounds(
            audio_length=6,
            bpm=BPM,
        )
    )
    return Song(
        [
            Track(FILTERED_SQUARE, notes),
        ],
        **SOUND_KWARGS,
    )
