from examples.wave_gens import FILTERED_SQUARE
from notes.chords import ChordType
from notes.note import Note, NoteLength
from notes.sound import Sound
from waves.songs import Metronome, Song, Track

BPM = 130
sounds = [
    Sound([Note("c4", custom_note_length=NoteLength.T3)], NoteLength.T6),
    ChordType.MINOR.as_sound(
        "f4",
        NoteLength.T6,
        additional_notes=-1,
    ),
    ChordType.MINOR.as_sound(
        "f4",
        NoteLength.T6,
        additional_notes=-1,
    ),
    Sound([Note("c4", custom_note_length=NoteLength.T3)], NoteLength.T6),
    ChordType.MINOR.as_sound(
        "f4",
        NoteLength.T6,
        additional_notes=-1,
    ),
    ChordType.MINOR.as_sound(
        "f4",
        NoteLength.T6,
        additional_notes=-1,
    ),
    Sound([Note("bb3", custom_note_length=NoteLength.T3)], NoteLength.T6),
    ChordType.MINOR.as_sound(
        "bb4",
        NoteLength.T6,
        inversion=-2,
    ),
    ChordType.MINOR.as_sound(
        "bb4",
        NoteLength.T6,
        inversion=-2,
    ),
    Sound([Note("c4", custom_note_length=NoteLength.T3)], NoteLength.T6),
    ChordType.MAJOR.as_sound(
        "c4",
        NoteLength.T6,
        additional_notes=1,
    ),
    ChordType.MAJOR.as_sound(
        "c4",
        NoteLength.T6,
        additional_notes=1,
    ),
]


def main():
    return Song(
        [
            Track(
                FILTERED_SQUARE,
                sounds,
            ),
            Metronome(
                measures=int(sum(sound.note_length.value for sound in sounds)),
                type=6,
            ),
        ],
        bpm=BPM,
    )
