from functools import partial

from examples.consts import SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE, FILTERED_SQUARE_BASS
from notes.chords import ChordType
from notes.note import Note, NoteLength
from notes.sound import Sound
from waves.manipulation import apply_volume
from waves.songs import Song, Track

CHORDS = (
    ChordType.MAJOR.as_sound(
        "a3",
        NoteLength.QUARTER,
    ).change_voicing([1, -1]),
    ChordType.MAJOR.as_sound(
        "a3",
        NoteLength.EIGHTH,
    ),
    ChordType.MAJOR.as_sound(
        "a3",
        NoteLength.EIGHTH,
        inversion=1,
    ),
    ChordType.MINOR.as_sound(
        "f#3",
        NoteLength.QUARTER + NoteLength.EIGHTH,
        inversion=1,
        additional_notes=4,
    ),
    Sound.rest(NoteLength.EIGHTH),
    ChordType.MAJOR.as_sound(
        "c#4",
        NoteLength.SIXTH,
        inversion=-1,
        additional_notes=5,
    ),
    ChordType.MAJOR.as_sound(
        "c#4",
        NoteLength.SIXTH,
        inversion=0,
        additional_notes=4,
    ),
    ChordType.MAJOR.as_sound(
        "c#4",
        NoteLength.SIXTH,
        inversion=1,
        additional_notes=4,
    ),
    ChordType.MAJOR.as_sound(
        "d4",
        NoteLength.SIXTH,
        inversion=-2,
        additional_notes=5,
    ),
    ChordType.MAJOR.as_sound(
        "d4",
        NoteLength.SIXTH + NoteLength.TWELFTH,
        inversion=-1,
        additional_notes=5,
    ),
    Sound.rest(NoteLength.TWELFTH),
)
BASS_NOTES = (
    Sound(
        [Note("a2")],
        NoteLength.QUARTER,
    ),
    Sound(
        [Note("c#3")],
        NoteLength.QUARTER,
    ),
    Sound(
        [Note("f#2")],
        NoteLength.QUARTER,
    ),
    Sound(
        [Note("a2")],
        NoteLength.QUARTER,
    ),
    Sound(
        [Note("c#2")],
        NoteLength.QUARTER,
    ),
    Sound(
        [Note("g#2")],
        NoteLength.QUARTER,
    ),
    Sound(
        [Note("d2")],
        NoteLength.QUARTER,
    ),
    Sound(
        [Note("f#2")],
        NoteLength.QUARTER,
    ),
)
MELODY = (
    Sound(
        [Note("a5")],
        NoteLength.D8,
    ),
    Sound(
        [Note("c#6")],
        NoteLength.D8,
    ),
    Sound(
        [Note("a5")],
        NoteLength.D8,
    ),
    Sound(
        [Note("c#6")],
        NoteLength.D8,
    ),
    Sound(
        [Note("a5")],
        NoteLength.D8,
    ),
    Sound(
        [Note("c#6")],
        NoteLength.D8,
    ),
    Sound(
        [Note("a5")],
        NoteLength.D8,
    ),
    Sound(
        [Note("c#6")],
        NoteLength.D8,
    ),
    # ----
    Sound(
        [Note("ab5")],
        NoteLength.D8,
    ),
    Sound(
        [Note("c#6")],
        NoteLength.D8,
    ),
    Sound(
        [Note("ab5")],
        NoteLength.D8,
    ),
    Sound(
        [Note("c#6")],
        NoteLength.D8,
    ),
    Sound(
        [Note("a5")],
        NoteLength.D16,
    ),
    Sound.rest(NoteLength.D16),
    Sound(
        [Note("a5", volume=125)],
        NoteLength.D16,
    ),
    Sound(
        [Note("ab5", volume=125)],
        NoteLength.D16,
    ),
    Sound(
        [Note("a5", volume=125)],
        NoteLength.D16,
    ),
    Sound(
        [Note("ab5", volume=125)],
        NoteLength.D16,
    ),
    Sound(
        [Note("a5", volume=125)],
        NoteLength.D16,
    ),
    Sound.rest(NoteLength.D16),
)


def main():
    return Song(
        [
            Track(
                FILTERED_SQUARE,
                CHORDS,
                pipeline=[
                    partial(apply_volume, volume=90),
                ],
            ),
            Track(
                FILTERED_SQUARE_BASS,
                BASS_NOTES,
            ),
            Track(
                FILTERED_SQUARE,
                MELODY,
            ),
        ],
        **(SOUND_KWARGS | {"repeat": 2, "bpm": 140}),
    )
