from functools import partial

import numpy as np

from examples.consts import SOUND_KWARGS
from examples.wave_gens import SQUARE
from notes.chords import ChordType
from notes.note import NoteLength
from notes.sound import Sound
from waves.generators import PipelineWaveGenerator
from waves.manipulation import smooth_low_pass_filter
from waves.songs import Song, Track

TEST_NOTE = ChordType.MINOR.as_sound(
    "a1",
    NoteLength.D2,
    additional_notes=3 * 6,
)
freqs = np.geomspace(
    start=20000,
    stop=20,
    num=10,
)
saws = [
    PipelineWaveGenerator(
        SQUARE,
        pipeline=[
            partial(
                smooth_low_pass_filter,
                frequency=freq,
                # band_width=bw,
            ),
        ],
    )
    for freq in freqs
] + [SQUARE]


def main():
    return Song(
        [
            Track(saw, sounds=[Sound.rest(NoteLength.D2)] * i + [TEST_NOTE])
            for i, saw in enumerate(saws)
        ],
        **SOUND_KWARGS,
    )
