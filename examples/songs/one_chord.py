from examples.consts import SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE
from notes.chords import ChordType
from notes.note import NoteLength
from waves.songs import Song, Track

ONE_CHORD = [
    ChordType.MAJOR.as_sound(
        "a4",
        NoteLength.HALF,
    ),
]


def main():
    return Song(
        [
            Track(FILTERED_SQUARE, ONE_CHORD),
        ],
        **SOUND_KWARGS,
    )
