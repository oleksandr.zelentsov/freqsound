from examples.wave_gens import FILTERED_SQUARE
from notes.note import Note, NoteLength
from notes.sound import Sound
from waves.songs import Song, Track

notes = [
    Sound(
        [
            Note("f4", custom_note_length=NoteLength.D1),
            Note("ab4", volume=110),
            Note("c4", volume=120, custom_note_length=NoteLength.D1),
        ],
        note_length=NoteLength.D2,
    ),
    Sound(
        [
            Note("g4", volume=110),
        ],
        note_length=NoteLength.D2,
    ),
    Sound(
        [
            Note("bb4"),
            Note("f4"),
            Note("db4"),
        ],
        note_length=NoteLength.D2,
    ),
    Sound(
        [
            Note("eb4"),
            Note("ab4"),
            Note("c4"),
        ],
        note_length=NoteLength.D2,
    ),
    Sound(
        [
            Note("db4"),
            Note("f4", custom_note_length=NoteLength.D1),
            Note("ab4"),
        ],
        note_length=NoteLength.D2,
    ),
    Sound(
        [
            Note("c4"),
            Note("g4"),
        ],
        note_length=NoteLength.D2,
    ),
    Sound(
        [
            Note("c4", custom_note_length=NoteLength.D1 * 1.5),
            Note("e4"),
            Note("g4"),
        ],
        note_length=NoteLength.D2,
    ),
    Sound(
        [
            Note("ab4"),
            Note("f4"),
        ],
        note_length=NoteLength.D1,
    ),
]


def main():
    return Song(
        [
            Track(FILTERED_SQUARE, notes),
            # SoundMetronome(sounds=notes),
        ],
        bpm=85,
    )
