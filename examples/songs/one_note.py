from examples.consts import SOUND_KWARGS
from examples.wave_gens import FILTERED_SQUARE
from notes.note import Note, NoteLength
from notes.sound import Sound
from waves.songs import Song, Track

ONE_NOTE = [
    Sound(
        [
            Note(
                "a4",
                volume=10,
            )
        ],
        NoteLength.HALF,
    ),
]


def main():
    return Song(
        [
            Track(FILTERED_SQUARE, ONE_NOTE),
        ],
        **SOUND_KWARGS,
    )
