from functools import partial

from examples.consts import VOLUME
from waves.generators import (
    CombinedWaveGenerator,
    PipelineWaveGenerator,
    SineWaveGenerator,
    SquareWaveGenerator,
)
from waves.manipulation import (
    add_decay,
    apply_volume,
    decay_tail,
    smooth_band_pass_filter,
    smooth_high_pass_filter,
    smooth_low_pass_filter,
)

SINE = SineWaveGenerator()
SQUARE = SquareWaveGenerator()
FILTERED_SQUARE = PipelineWaveGenerator(
    SQUARE,
    pipeline=[
        partial(smooth_high_pass_filter, frequency=100),
        partial(smooth_low_pass_filter, frequency=4400),
    ],
    track_pipeline=[
        partial(apply_volume, volume=VOLUME),
    ],
)
SMOOTH_SQUARE = PipelineWaveGenerator(
    SQUARE,
    pipeline=[
        partial(smooth_band_pass_filter, frequency=4400, band_width=500),
    ],
)
FILTERED_SQUARE_BASS = PipelineWaveGenerator(
    SQUARE,
    track_pipeline=[
        partial(smooth_low_pass_filter, frequency=2200),
        partial(apply_volume, volume=VOLUME),
    ],
)
BASIC_BASS = CombinedWaveGenerator(
    [
        SineWaveGenerator(),
        PipelineWaveGenerator(
            SQUARE,
            track_pipeline=[
                # partial(cut_high_pass_filter, cut_frequency=200),
                partial(smooth_low_pass_filter, frequency=5000),
                # partial(apply_volume, volume=5),
            ],
        ),
    ]
)
DECAY_TAIL_SINE = PipelineWaveGenerator(
    SINE,
    [
        partial(decay_tail, tail_length=0.101),
    ],
)

DECAYED_SINE = PipelineWaveGenerator(
    SINE,
    [
        partial(add_decay, length=0.1),
    ],
)
