from notes.generators.note_generators import PassType, UpDownScaleNoteGenerator
from notes.generators.rhythm_generators import RandomMeasureSubdivision, RhythmRepeater
from notes.generators.sound_generators import SimpleSingleRandomSoundGenerator
from notes.note import Note, NoteLength
from notes.scales import Scale

NOTE_SCALE_GEN = SimpleSingleRandomSoundGenerator(
    UpDownScaleNoteGenerator(
        scale=Scale([0, 4, 7, 11, 12, 19]),
        beginning_note=Note("g3"),
        pass_type=PassType.UP_DOWN,
        more_octaves=0,
    ),
    RhythmRepeater(
        [
            NoteLength.D16,
        ]
        * 16,
    ),
)

SIMPLE_NOTE_COMPLEX_RHYTHM = SimpleSingleRandomSoundGenerator(
    UpDownScaleNoteGenerator(
        scale=Scale([0, 2, 3, 7, 8]),
        beginning_note=Note("b2"),
        pass_type=PassType.UP_DOWN,
    ),
    RandomMeasureSubdivision(repeat_times=1),
)
