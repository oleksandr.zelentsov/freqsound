from waves.scalers import scale_any_bit

VOLUME = 60
BPM = 140
TRANSPOSE = 0
REPEAT = 1
AUDIO_LENGTH = 15
SOUND_KWARGS = dict(
    bpm=BPM,
    transpose=TRANSPOSE,
    repeat=REPEAT,
    scaler=scale_any_bit,
)
