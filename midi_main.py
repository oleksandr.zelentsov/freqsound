import os

from main import main
from waves.midi import MidiConverter, play_midi

filename = os.getenv("MIDI_OUTPUT") or "out.mid"


def midi_output():
    song = main()
    midi = MidiConverter(song)
    mf = midi.to_midi()
    mf.save(filename)
    play_midi(filename)


if __name__ == "__main__":
    midi_output()
