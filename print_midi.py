from os import getenv

from waves.midi import print_midi_notes

if __name__ == "__main__":
    print_midi_notes(getenv("MIDI_INPUT") or "correct.mid")
